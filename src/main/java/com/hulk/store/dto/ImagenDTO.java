package com.hulk.store.dto;

import com.hulk.store.domain.Imagen;
import com.hulk.store.dto.audit.AuditDTO;
import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class ImagenDTO extends AuditDTO {

	private Long id;
	private String descripcion;
	private String nombre;
	private String url;
	private String tipo;
	private Long productoId;

	public ImagenDTO() {
	}
 	public ImagenDTO(Imagen imagen) {
		this.setId(imagen.getId());
		this.setDescripcion(imagen.getDescripcion());
		this.setNombre(imagen.getNombre());
		this.setUrl(imagen.getUrl());
		this.setTipo(imagen.getTipo());
		this.setCreatedBy(imagen.getCreatedBy());
		this.setCreatedDate(imagen.getCreatedDate());
		this.setLastModifiedBy(imagen.getLastModifiedBy());
		this.setLastModifiedDate(imagen.getLastModifiedDate());
		this.setProductoId(imagen.getProducto() != null ? imagen.getProducto().getId() : null);
	}

}
