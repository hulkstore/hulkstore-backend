package com.hulk.store.dto;

import java.util.Set;
import com.hulk.store.dto.audit.AuditDTO;
import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class UsuarioDTO extends AuditDTO {

	private Long id;
	private String nombre;
	private Long nroTarjeta;
	private String username;
	private String password;
	private String email;
	private Boolean estado = false;
	private Set<String> roles;

}
