package com.hulk.store.dto;

import com.hulk.store.dto.audit.AuditDTO;

import lombok.*;

@Getter
@Setter
@ToString
public class CategoriaProductoDTO extends AuditDTO {

	private Long id;
	private String nombre;

}
