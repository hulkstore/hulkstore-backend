package com.hulk.store.dto;

import com.hulk.store.domain.ProductoCamiseta;
import com.hulk.store.dto.audit.AuditDTO;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class ProductoCamisetaDTO extends AuditDTO {

	private Long id;
	private String sexo;
	private Long colorId; // foreign key
	private Long tallaId; // foreign key
	private Long productoId; // foreign key

	public ProductoCamisetaDTO() {
	}
	
 	public ProductoCamisetaDTO(ProductoCamiseta productoCamiseta) {
		this.setId(productoCamiseta.getId());
		this.setSexo(productoCamiseta.getSexo());
		this.setCreatedBy(productoCamiseta.getCreatedBy());
		this.setCreatedDate(productoCamiseta.getCreatedDate());
		this.setLastModifiedBy(productoCamiseta.getLastModifiedBy());
		this.setLastModifiedDate(productoCamiseta.getLastModifiedDate());
		this.setColorId(productoCamiseta.getColor() != null ? productoCamiseta.getColor().getId() : null);
		this.setTallaId(productoCamiseta.getTalla() != null ? productoCamiseta.getTalla().getId() : null);
		this.setProductoId(productoCamiseta.getProducto() != null ? productoCamiseta.getProducto().getId() : null);
	}

}
