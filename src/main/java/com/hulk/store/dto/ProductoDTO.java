package com.hulk.store.dto;

import java.util.List;

import com.hulk.store.domain.Imagen;
import com.hulk.store.domain.Producto;
import com.hulk.store.dto.audit.AuditDTO;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class ProductoDTO extends AuditDTO {

	private Long id;
	private String nombre;
	private String descripcion;
	private String codigo;
	private Double costoUnitarioCompra;
	private Double precioUnitarioVenta;
	private Integer cantidad;
	private Boolean estado;
	private Long categoriaId; // foreign key
	private Long etiquetaId; // foreign key
	// private Long imagenId; // foreign key
	private ProductoCamisetaDTO productoCamisetaDTO;
	private ProductoComicDTO productoComicDTO;
    private List<ImagenDTO> imagenDTOs;

}
