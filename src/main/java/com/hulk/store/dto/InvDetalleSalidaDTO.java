package com.hulk.store.dto;

import com.hulk.store.dto.audit.AuditDTO;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class InvDetalleSalidaDTO extends AuditDTO {

	private Long id;
	private Integer cantidad;
	private Double precioUnitarioVenta;
	private Long productoId; // foreign key
	private Long invSalidaId; // foreign key
}
