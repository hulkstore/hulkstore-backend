package com.hulk.store.dto;

import com.hulk.store.dto.audit.AuditDTO;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class InvEntradaDTO extends AuditDTO {

	private Long id;
	private Boolean estado; // foreign key
	private Double total;
}
