package com.hulk.store.dto;

import com.hulk.store.domain.ProductoCamiseta;
import com.hulk.store.domain.ProductoComic;
import com.hulk.store.dto.audit.AuditDTO;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class ProductoComicDTO extends AuditDTO {

	private Long id;
	private String titulo;
	private String genero;
	private Integer volumen;
	private Integer anioLanzamiento;
	private String idioma;
	private Long productoId; // foreign key

	public ProductoComicDTO() {
	}
	
 	public ProductoComicDTO(ProductoComic productocomic) {
		this.setId(productocomic.getId());
		this.setTitulo(productocomic.getTitulo());
		this.setGenero(productocomic.getGenero());
		this.setVolumen(productocomic.getVolumen());
		this.setAnioLanzamiento(productocomic.getAnioLanzamiento());
		this.setIdioma(productocomic.getIdioma());
		this.setCreatedBy(productocomic.getCreatedBy());
		this.setCreatedDate(productocomic.getCreatedDate());
		this.setLastModifiedBy(productocomic.getLastModifiedBy());
		this.setLastModifiedDate(productocomic.getLastModifiedDate());
		this.setProductoId(productocomic.getProducto() != null ? productocomic.getProducto().getId() : null);
	}

}
