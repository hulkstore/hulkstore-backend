package com.hulk.store.dto; 

import com.hulk.store.dto.audit.AuditDTO;
import lombok.*; 

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class EtiquetaProductoDTO extends AuditDTO  {

	private Long id;
	private String nombre;
	private String descripcion;
	
} 
