package com.hulk.store.dto;

import com.hulk.store.dto.audit.AuditDTO;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class InvDetalleEntradaDTO extends AuditDTO {

	private Long id;
	private Double costoUnitario;
	private Integer cantidad;
	private Double subTotal;
	private Long productoId; // foreign key
	private Long invEntradaId;

}
