package com.hulk.store.dto; 

import com.hulk.store.dto.audit.AuditDTO;

import lombok.*; 

@EqualsAndHashCode(callSuper = false) 
@Getter
@Setter
@ToString
public class InvSalidaDTO extends AuditDTO {

	private Long id;
	private Double descuento;
	private Double montoTotal;
	private String tipoPago;
	private Boolean estado; // foreign key

} 
