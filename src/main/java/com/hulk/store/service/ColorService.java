package com.hulk.store.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Color;
import com.hulk.store.dto.ColorDTO;
import com.hulk.store.repository.ColorRepository;
import com.hulk.store.service.mapper.ColorMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;

@Service
public class ColorService {

	private final ColorRepository colorRepository;

	private final ColorMapper colorMapper;

	public ColorService(ColorRepository colorRepository, ColorMapper colorMapper) {
		this.colorRepository = colorRepository;
		this.colorMapper = colorMapper;
	}

	public ColorDTO save(ColorDTO colorDTO) {
		Color color = this.colorMapper.toEntity(colorDTO);
		this.audit(color);
		colorDTO = this.colorMapper.toDTO(this.colorRepository.save(color));
		return colorDTO;
	}

	public Optional<ColorDTO> findOne(Long colorId) {
		return this.colorRepository.findById(colorId).map(t -> this.colorMapper.toDTO(t));
	}

	public Page<ColorDTO> findAll(Pageable pageable) {
		return this.colorRepository.findAll(pageable).map(t -> this.colorMapper.toDTO(t));
	}

	public void deleteOne(Long colorId) {
		this.colorRepository.deleteById(colorId);
	}

	public void audit(Color color) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (color.getCreatedBy() == null) {
			color.setCreatedBy(currentUser);
			color.setCreatedDate(Instant.now());
		} else {
			color.setLastModifiedBy(currentUser);
			color.setLastModifiedDate(Instant.now());
		}
	}

	@Transactional(readOnly = true)
    public Page<ColorDTO> searchByKeyword(String keyword, Pageable pageable) {
        String query = "%" + keyword + "%";
        return this.colorRepository.search(query, pageable).map(t -> this.colorMapper.toDTO(t));
    }

}