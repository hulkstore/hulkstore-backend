package com.hulk.store.service.mapper;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.hulk.store.domain.Rol;
import com.hulk.store.domain.Usuario;
import com.hulk.store.dto.UsuarioDTO;

@Service
public class UsuarioMapper {

	public Usuario toEntity(UsuarioDTO usuarioDTO) {
		Usuario usuario = new Usuario();

		usuario.setId(usuarioDTO.getId());
		usuario.setNombre(usuarioDTO.getNombre());
		usuario.setNroTarjeta(usuarioDTO.getNroTarjeta());
		usuario.setUsername(usuarioDTO.getUsername());
		usuario.setPassword(usuarioDTO.getPassword());
		usuario.setEmail(usuarioDTO.getEmail());
		usuario.setEstado(usuarioDTO.getEstado());
		Set<Rol> roles = this.authoritiesFromStrings(usuarioDTO.getRoles());
		usuario.setRoles(roles);
		usuario.setCreatedBy(usuarioDTO.getCreatedBy());
		usuario.setCreatedDate(usuarioDTO.getCreatedDate());
		usuario.setLastModifiedBy(usuarioDTO.getLastModifiedBy());
		usuario.setLastModifiedDate(usuarioDTO.getLastModifiedDate());
		return usuario;
	}

	public UsuarioDTO toDTO(Usuario usuario) {
		UsuarioDTO usuarioDTO = new UsuarioDTO();

		usuarioDTO.setId(usuario.getId());
		usuarioDTO.setNombre(usuario.getNombre());
		usuarioDTO.setNroTarjeta(usuario.getNroTarjeta());
		usuarioDTO.setUsername(usuario.getUsername());
		usuarioDTO.setPassword(usuario.getPassword());
		usuarioDTO.setEmail(usuario.getEmail());
		usuarioDTO.setEstado(usuario.getEstado());
        usuarioDTO.setRoles(usuario.getRoles().stream().map(Rol::getNombre).collect(Collectors.toSet()));
		usuarioDTO.setCreatedBy(usuario.getCreatedBy());
		usuarioDTO.setCreatedDate(usuario.getCreatedDate());
		usuarioDTO.setLastModifiedBy(usuario.getLastModifiedBy());
		usuarioDTO.setLastModifiedDate(usuario.getLastModifiedDate());
		return usuarioDTO;
	}

    private Set<Rol> authoritiesFromStrings(Set<String> authoritiesAsString) {
        Set<Rol> authorities = new HashSet<>();

        if (authoritiesAsString != null) {
            authorities =
                authoritiesAsString
                    .stream()
                    .map(string -> {
                        Rol auth = new Rol();
                        auth.setNombre(string);
                        return auth;
                    })
                    .collect(Collectors.toSet());
        }

        return authorities;
    }

    public Usuario userFromId(Long id) {
        if (id == null) {
            return null;
        }
        Usuario user = new Usuario();
        user.setId(id);
        return user;
    }

}
