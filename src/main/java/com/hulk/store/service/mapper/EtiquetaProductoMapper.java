package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;

import com.hulk.store.domain.EtiquetaProducto;
import com.hulk.store.dto.EtiquetaProductoDTO;

@Service
public class EtiquetaProductoMapper {

	public EtiquetaProducto toEntity(EtiquetaProductoDTO etiquetaproductoDTO) {
		EtiquetaProducto etiquetaproducto = new EtiquetaProducto();
		
		etiquetaproducto.setId(etiquetaproductoDTO.getId());
		etiquetaproducto.setDescripcion(etiquetaproductoDTO.getDescripcion());
		etiquetaproducto.setNombre(etiquetaproductoDTO.getNombre());
		etiquetaproducto.setCreatedBy(etiquetaproductoDTO.getCreatedBy());
		etiquetaproducto.setCreatedDate(etiquetaproductoDTO.getCreatedDate());
		etiquetaproducto.setLastModifiedBy(etiquetaproductoDTO.getLastModifiedBy());
		etiquetaproducto.setLastModifiedDate(etiquetaproductoDTO.getLastModifiedDate());
		return etiquetaproducto;
	}

	public EtiquetaProductoDTO toDTO(EtiquetaProducto etiquetaproducto) {
		EtiquetaProductoDTO etiquetaproductoDTO = new EtiquetaProductoDTO();
		
		etiquetaproductoDTO.setId(etiquetaproducto.getId());
		etiquetaproductoDTO.setDescripcion(etiquetaproducto.getDescripcion());
		etiquetaproductoDTO.setNombre(etiquetaproducto.getNombre());
		etiquetaproductoDTO.setCreatedBy(etiquetaproducto.getCreatedBy());
		etiquetaproductoDTO.setCreatedDate(etiquetaproducto.getCreatedDate());
		etiquetaproductoDTO.setLastModifiedBy(etiquetaproducto.getLastModifiedBy());
		etiquetaproductoDTO.setLastModifiedDate(etiquetaproducto.getLastModifiedDate());
		return etiquetaproductoDTO;
	}

}
