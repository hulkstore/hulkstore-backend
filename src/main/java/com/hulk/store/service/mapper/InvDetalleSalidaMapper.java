package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;

import com.hulk.store.domain.InvDetalleSalida;
import com.hulk.store.domain.InvSalida;
import com.hulk.store.dto.InvDetalleSalidaDTO;
import com.hulk.store.domain.Producto;

@Service
public class InvDetalleSalidaMapper {

	public InvDetalleSalida toEntity(InvDetalleSalidaDTO invdetallesalidaDTO) {
		InvDetalleSalida invdetallesalida = new InvDetalleSalida();

		invdetallesalida.setId(invdetallesalidaDTO.getId());
		invdetallesalida.setCantidad(invdetallesalidaDTO.getCantidad());
		invdetallesalida.setPrecioUnitarioVenta(invdetallesalidaDTO.getPrecioUnitarioVenta());
		invdetallesalida.setCreatedBy(invdetallesalidaDTO.getCreatedBy());
		invdetallesalida.setCreatedDate(invdetallesalidaDTO.getCreatedDate());
		invdetallesalida.setLastModifiedBy(invdetallesalidaDTO.getLastModifiedBy());
		invdetallesalida.setLastModifiedDate(invdetallesalidaDTO.getLastModifiedDate());
		if (invdetallesalidaDTO.getId() != null) {
			Producto producto = new Producto();
			producto.setId(invdetallesalidaDTO.getId());
			invdetallesalida.setProducto(producto);
		}
		if (invdetallesalidaDTO.getId() != null) {
			InvSalida invSalida = new InvSalida();
			invSalida.setId(invdetallesalidaDTO.getId());
			invdetallesalida.setInvSalida(invSalida);
		}
		return invdetallesalida;
	}

	public InvDetalleSalidaDTO toDTO(InvDetalleSalida invdetallesalida) {
		InvDetalleSalidaDTO invdetallesalidaDTO = new InvDetalleSalidaDTO();

		invdetallesalidaDTO.setId(invdetallesalida.getId());
		invdetallesalidaDTO.setCantidad(invdetallesalida.getCantidad());
		invdetallesalidaDTO.setPrecioUnitarioVenta(invdetallesalida.getPrecioUnitarioVenta());
		invdetallesalidaDTO.setCreatedBy(invdetallesalida.getCreatedBy());
		invdetallesalidaDTO.setCreatedDate(invdetallesalida.getCreatedDate());
		invdetallesalidaDTO.setLastModifiedBy(invdetallesalida.getLastModifiedBy());
		invdetallesalidaDTO.setLastModifiedDate(invdetallesalida.getLastModifiedDate());
		invdetallesalidaDTO
				.setProductoId(invdetallesalida.getProducto() != null ? invdetallesalida.getProducto().getId() : null);
		invdetallesalidaDTO
				.setInvSalidaId(invdetallesalida.getInvSalida() != null ? invdetallesalida.getInvSalida().getId() : null);
		return invdetallesalidaDTO;
	}

}
