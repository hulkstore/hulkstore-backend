package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;

import com.hulk.store.domain.InvSalida;
import com.hulk.store.dto.InvSalidaDTO;

@Service
public class InvSalidaMapper {

	public InvSalida toEntity(InvSalidaDTO invsalidaDTO) {
		InvSalida invsalida = new InvSalida();

		invsalida.setId(invsalidaDTO.getId());
		invsalida.setDescuento(invsalidaDTO.getDescuento());
		invsalida.setMontoTotal(invsalidaDTO.getMontoTotal());
		invsalida.setTipoPago(invsalidaDTO.getTipoPago());
		invsalida.setEstado(invsalidaDTO.getEstado());
		invsalida.setCreatedBy(invsalidaDTO.getCreatedBy());
		invsalida.setCreatedDate(invsalidaDTO.getCreatedDate());
		invsalida.setLastModifiedBy(invsalidaDTO.getLastModifiedBy());
		invsalida.setLastModifiedDate(invsalidaDTO.getLastModifiedDate());
		return invsalida;
	}

	public InvSalidaDTO toDTO(InvSalida invsalida) {
		InvSalidaDTO invsalidaDTO = new InvSalidaDTO();

		invsalidaDTO.setId(invsalida.getId());
		invsalidaDTO.setDescuento(invsalida.getDescuento());
		invsalidaDTO.setMontoTotal(invsalida.getMontoTotal());
		invsalidaDTO.setTipoPago(invsalida.getTipoPago());
		invsalidaDTO.setEstado(invsalida.getEstado());
		invsalidaDTO.setCreatedBy(invsalida.getCreatedBy());
		invsalidaDTO.setCreatedDate(invsalida.getCreatedDate());
		invsalidaDTO.setLastModifiedBy(invsalida.getLastModifiedBy());
		invsalidaDTO.setLastModifiedDate(invsalida.getLastModifiedDate());
		return invsalidaDTO;
	}

}
