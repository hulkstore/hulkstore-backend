package com.hulk.store.service.mapper;

import com.hulk.store.domain.Producto;
import com.hulk.store.domain.ProductoCamiseta;
import com.hulk.store.domain.ProductoComic;
import com.hulk.store.dto.ImagenDTO;
import com.hulk.store.dto.ProductoCamisetaDTO;
import com.hulk.store.dto.ProductoComicDTO;
import com.hulk.store.dto.ProductoDTO;

import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.hulk.store.domain.CategoriaProducto;
import com.hulk.store.domain.EtiquetaProducto;
import com.hulk.store.domain.Imagen;

@Service
public class ProductoMapper {

	public Producto toEntity(ProductoDTO productoDTO) {
		Producto producto = new Producto();

		producto.setId(productoDTO.getId());
		producto.setNombre(productoDTO.getNombre());
		producto.setDescripcion(productoDTO.getDescripcion());
		producto.setCodigo(productoDTO.getCodigo());
		producto.setCostoUnitarioCompra(productoDTO.getCostoUnitarioCompra());
		producto.setPrecioUnitarioVenta(productoDTO.getPrecioUnitarioVenta());
		producto.setCantidad(productoDTO.getCantidad());
		producto.setEstado(productoDTO.getEstado());
		producto.setCreatedBy(productoDTO.getCreatedBy());
		producto.setCreatedDate(productoDTO.getCreatedDate());
		producto.setLastModifiedBy(productoDTO.getLastModifiedBy());
		producto.setLastModifiedDate(productoDTO.getLastModifiedDate());
		if (productoDTO.getCategoriaId() != null) {
			CategoriaProducto categoriaProducto = new CategoriaProducto();
			categoriaProducto.setId(productoDTO.getCategoriaId());
			producto.setCategoria(categoriaProducto);
		}
		if (productoDTO.getEtiquetaId() != null) {
			EtiquetaProducto etiquetaProducto = new EtiquetaProducto();
			etiquetaProducto.setId(productoDTO.getEtiquetaId());
			producto.setEtiqueta(etiquetaProducto);
		}
		producto.setImagenes(null);
		return producto;
	}

	public ProductoDTO toDTO(Producto producto) {
		ProductoDTO productoDTO = new ProductoDTO();

		productoDTO.setId(producto.getId());
		productoDTO.setNombre(producto.getNombre());
		productoDTO.setDescripcion(producto.getDescripcion());
		productoDTO.setCodigo(producto.getCodigo());
		productoDTO.setCostoUnitarioCompra(producto.getCostoUnitarioCompra());
		productoDTO.setPrecioUnitarioVenta(producto.getPrecioUnitarioVenta());
		productoDTO.setCantidad(producto.getCantidad());
		productoDTO.setEstado(producto.getEstado());
		productoDTO.setCreatedBy(producto.getCreatedBy());
		productoDTO.setCreatedDate(producto.getCreatedDate());
		productoDTO.setLastModifiedBy(producto.getLastModifiedBy());
		productoDTO.setLastModifiedDate(producto.getLastModifiedDate());
		productoDTO.setCategoriaId(producto.getCategoria() != null ? producto.getCategoria().getId() : null);
		productoDTO.setEtiquetaId(producto.getEtiqueta() != null ? producto.getEtiqueta().getId() : null);
		productoDTO.setProductoCamisetaDTO(producto.getProductoCamiseta() != null ? new ProductoCamisetaDTO(producto.getProductoCamiseta())  : null);
		productoDTO.setProductoComicDTO(producto.getProductoComic() != null ? new ProductoComicDTO(producto.getProductoComic()) : null);
		productoDTO.setImagenDTOs(producto.getImagenes() != null ? producto.getImagenes().stream().map(ImagenDTO::new).collect(Collectors.toList()) : null);
		return productoDTO;
	}

}
