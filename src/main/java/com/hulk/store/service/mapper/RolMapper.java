package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;
import com.hulk.store.domain.Rol;
import com.hulk.store.dto.RolDTO;

@Service
public class RolMapper {
  public Rol toEntity(RolDTO rolDTO) {
    Rol rol = new Rol();

    rol.setNombre(rolDTO.getNombre());
    return rol;
  }

  public RolDTO toDTO(Rol rol) {
    RolDTO rolDTO = new RolDTO();

    rolDTO.setNombre(rol.getNombre());
    return rolDTO;
  }
}
