package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;
import com.hulk.store.domain.Producto;
import com.hulk.store.domain.Imagen;
import com.hulk.store.dto.ImagenDTO;

@Service
public class ImagenMapper {

	public Imagen toEntity(ImagenDTO imagenDTO) {
		Imagen imagen = new Imagen();

		imagen.setId(imagenDTO.getId());
		imagen.setDescripcion(imagenDTO.getDescripcion());
		imagen.setNombre(imagenDTO.getNombre());
		imagen.setUrl(imagenDTO.getUrl());
		imagen.setTipo(imagenDTO.getTipo());
		imagen.setCreatedBy(imagenDTO.getCreatedBy());
		imagen.setCreatedDate(imagenDTO.getCreatedDate());
		imagen.setLastModifiedBy(imagenDTO.getLastModifiedBy());
		imagen.setLastModifiedDate(imagenDTO.getLastModifiedDate());
		if (imagenDTO.getProductoId() != null) {
			Producto producto = new Producto();
			producto.setId(imagenDTO.getId());
			imagen.setProducto(producto);
		}
		return imagen;
	}

	public ImagenDTO toDTO(Imagen imagen) {
		ImagenDTO imagenDTO = new ImagenDTO();

		imagenDTO.setId(imagen.getId());
		imagenDTO.setDescripcion(imagen.getDescripcion());
		imagenDTO.setNombre(imagen.getNombre());
		imagenDTO.setUrl(imagen.getUrl());
		imagenDTO.setTipo(imagen.getTipo());
		imagenDTO.setCreatedBy(imagen.getCreatedBy());
		imagenDTO.setCreatedDate(imagen.getCreatedDate());
		imagenDTO.setLastModifiedBy(imagen.getLastModifiedBy());
		imagenDTO.setLastModifiedDate(imagen.getLastModifiedDate());
		imagenDTO.setProductoId(imagen.getProducto() != null ? imagen.getProducto().getId() : null);
		return imagenDTO;
	}

}
