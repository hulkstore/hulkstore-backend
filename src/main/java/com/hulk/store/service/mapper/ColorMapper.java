package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;

import com.hulk.store.domain.Color;
import com.hulk.store.dto.ColorDTO;

@Service
public class ColorMapper {

	public Color toEntity(ColorDTO colorDTO) {
		Color color = new Color();
		
		color.setId(colorDTO.getId());
		color.setNombre(colorDTO.getNombre());
		color.setCodigo(colorDTO.getCodigo());
		color.setCreatedBy(colorDTO.getCreatedBy());
		color.setCreatedDate(colorDTO.getCreatedDate());
		color.setLastModifiedBy(colorDTO.getLastModifiedBy());
		color.setLastModifiedDate(colorDTO.getLastModifiedDate());
		return color;
	}
	
	public ColorDTO toDTO(Color color) {
		ColorDTO colorDTO = new ColorDTO();
		
		colorDTO.setId(color.getId());
		colorDTO.setNombre(color.getNombre());
		colorDTO.setCodigo(color.getCodigo());
		colorDTO.setCreatedBy(color.getCreatedBy());
		colorDTO.setCreatedDate(color.getCreatedDate());
		colorDTO.setLastModifiedBy(color.getLastModifiedBy());
		colorDTO.setLastModifiedDate(color.getLastModifiedDate());
		return colorDTO;
	}

}
