package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;
import com.hulk.store.domain.InvEntrada;
import com.hulk.store.domain.InvDetalleEntrada;
import com.hulk.store.dto.InvDetalleEntradaDTO;
import com.hulk.store.domain.Producto;

@Service
public class InvDetalleEntradaMapper {

	public InvDetalleEntrada toEntity(InvDetalleEntradaDTO invdetalleentradaDTO) {
		InvDetalleEntrada invdetalleentrada = new InvDetalleEntrada();

		invdetalleentrada.setId(invdetalleentradaDTO.getId());
		invdetalleentrada.setCostoUnitario(invdetalleentradaDTO.getCostoUnitario());
		invdetalleentrada.setCantidad(invdetalleentradaDTO.getCantidad());
		invdetalleentrada.setCreatedBy(invdetalleentradaDTO.getCreatedBy());
		invdetalleentrada.setCreatedDate(invdetalleentradaDTO.getCreatedDate());
		invdetalleentrada.setLastModifiedBy(invdetalleentradaDTO.getLastModifiedBy());
		invdetalleentrada.setLastModifiedDate(invdetalleentradaDTO.getLastModifiedDate());
		if (invdetalleentradaDTO.getProductoId() != null) {
			Producto producto = new Producto();
			producto.setId(invdetalleentradaDTO.getProductoId());
			invdetalleentrada.setProducto(producto);
		}
		if (invdetalleentradaDTO.getInvEntradaId() != null) {
			InvEntrada invEntrada = new InvEntrada();
			invEntrada.setId(invdetalleentradaDTO.getInvEntradaId());
			invdetalleentrada.setInvEntrada(invEntrada);
		}
		return invdetalleentrada;
	}

	public InvDetalleEntradaDTO toDTO(InvDetalleEntrada invdetalleentrada) {
		InvDetalleEntradaDTO invdetalleentradaDTO = new InvDetalleEntradaDTO();

		invdetalleentradaDTO.setId(invdetalleentrada.getId());
		invdetalleentradaDTO.setCostoUnitario(invdetalleentrada.getCostoUnitario());
		invdetalleentradaDTO.setCantidad(invdetalleentrada.getCantidad());
		invdetalleentradaDTO.setCreatedBy(invdetalleentrada.getCreatedBy());
		invdetalleentradaDTO.setCreatedDate(invdetalleentrada.getCreatedDate());
		invdetalleentradaDTO.setLastModifiedBy(invdetalleentrada.getLastModifiedBy());
		invdetalleentradaDTO.setLastModifiedDate(invdetalleentrada.getLastModifiedDate());
		invdetalleentradaDTO
				.setProductoId(invdetalleentrada.getProducto() != null ? invdetalleentrada.getProducto().getId() : null);
		invdetalleentradaDTO
				.setInvEntradaId(invdetalleentrada.getInvEntrada() != null ? invdetalleentrada.getInvEntrada().getId() : null);
		return invdetalleentradaDTO;
	}

}
