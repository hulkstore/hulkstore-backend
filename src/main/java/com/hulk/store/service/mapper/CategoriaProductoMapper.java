package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;
import com.hulk.store.domain.CategoriaProducto;
import com.hulk.store.dto.CategoriaProductoDTO;

@Service
public class CategoriaProductoMapper {

	public CategoriaProducto toEntity(CategoriaProductoDTO categoriaproductoDTO) {
		CategoriaProducto categoriaproducto = new CategoriaProducto();
		
		categoriaproducto.setId(categoriaproductoDTO.getId());
		categoriaproducto.setNombre(categoriaproductoDTO.getNombre());
		categoriaproducto.setCreatedBy(categoriaproductoDTO.getCreatedBy());
		categoriaproducto.setCreatedDate(categoriaproductoDTO.getCreatedDate());
		categoriaproducto.setLastModifiedBy(categoriaproductoDTO.getLastModifiedBy());
		categoriaproducto.setLastModifiedDate(categoriaproductoDTO.getLastModifiedDate());
		return categoriaproducto;
	}

	public CategoriaProductoDTO toDTO(CategoriaProducto categoriaproducto) {
		CategoriaProductoDTO categoriaproductoDTO = new CategoriaProductoDTO();
		
		categoriaproductoDTO.setId(categoriaproducto.getId());
		categoriaproductoDTO.setNombre(categoriaproducto.getNombre());
		categoriaproductoDTO.setCreatedBy(categoriaproducto.getCreatedBy());
		categoriaproductoDTO.setCreatedDate(categoriaproducto.getCreatedDate());
		categoriaproductoDTO.setLastModifiedBy(categoriaproducto.getLastModifiedBy());
		categoriaproductoDTO.setLastModifiedDate(categoriaproducto.getLastModifiedDate());
		return categoriaproductoDTO;
	}
	
}
