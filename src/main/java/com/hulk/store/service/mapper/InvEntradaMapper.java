package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;

import com.hulk.store.domain.InvEntrada;
import com.hulk.store.dto.InvEntradaDTO;

@Service
public class InvEntradaMapper {

	public InvEntrada toEntity(InvEntradaDTO inventradaDTO) {
		InvEntrada inventrada = new InvEntrada();

		inventrada.setId(inventradaDTO.getId());
		inventrada.setEstado(inventradaDTO.getEstado());
		inventrada.setTotal(inventradaDTO.getTotal());
		inventrada.setCreatedBy(inventradaDTO.getCreatedBy());
		inventrada.setCreatedDate(inventradaDTO.getCreatedDate());
		inventrada.setLastModifiedBy(inventradaDTO.getLastModifiedBy());
		inventrada.setLastModifiedDate(inventradaDTO.getLastModifiedDate());
		return inventrada;
	}

	public InvEntradaDTO toDTO(InvEntrada inventrada) {
		InvEntradaDTO inventradaDTO = new InvEntradaDTO();

		inventradaDTO.setId(inventrada.getId());
		inventradaDTO.setEstado(inventrada.getEstado());
		inventradaDTO.setTotal(inventrada.getTotal());
		inventradaDTO.setCreatedBy(inventrada.getCreatedBy());
		inventradaDTO.setCreatedDate(inventrada.getCreatedDate());
		inventradaDTO.setLastModifiedBy(inventrada.getLastModifiedBy());
		inventradaDTO.setLastModifiedDate(inventrada.getLastModifiedDate());
		return inventradaDTO;
	}

}
