package com.hulk.store.service.mapper;

import com.hulk.store.domain.ProductoCamiseta;
import com.hulk.store.dto.ProductoCamisetaDTO;
import org.springframework.stereotype.Service;

import com.hulk.store.domain.Color;
import com.hulk.store.domain.Talla;
import com.hulk.store.domain.Producto;

@Service
public class ProductoCamisetaMapper {

	public ProductoCamiseta toEntity(ProductoCamisetaDTO productocamisetaDTO) {
		ProductoCamiseta productocamiseta = new ProductoCamiseta();

		productocamiseta.setId(productocamisetaDTO.getId());
		productocamiseta.setSexo(productocamisetaDTO.getSexo());
		productocamiseta.setCreatedBy(productocamisetaDTO.getCreatedBy());
		productocamiseta.setCreatedDate(productocamisetaDTO.getCreatedDate());
		productocamiseta.setLastModifiedBy(productocamisetaDTO.getLastModifiedBy());
		productocamiseta.setLastModifiedDate(productocamisetaDTO.getLastModifiedDate());
		if (productocamisetaDTO.getColorId() != null) {
			Color color = new Color();
			color.setId(productocamisetaDTO.getColorId());
			productocamiseta.setColor(color);
		}
		if (productocamisetaDTO.getTallaId() != null) {
			Talla talla = new Talla();
			talla.setId(productocamisetaDTO.getTallaId());
			productocamiseta.setTalla(talla);
		}
		if (productocamisetaDTO.getProductoId() != null) {
			Producto producto = new Producto();
			producto.setId(productocamisetaDTO.getProductoId());
			productocamiseta.setProducto(producto);
		}
		return productocamiseta;
	}

	public ProductoCamisetaDTO toDTO(ProductoCamiseta productocamiseta) {
		ProductoCamisetaDTO productocamisetaDTO = new ProductoCamisetaDTO();

		productocamisetaDTO.setId(productocamiseta.getId());
		productocamisetaDTO.setSexo(productocamiseta.getSexo());
		productocamisetaDTO.setCreatedBy(productocamiseta.getCreatedBy());
		productocamisetaDTO.setCreatedDate(productocamiseta.getCreatedDate());
		productocamisetaDTO.setLastModifiedBy(productocamiseta.getLastModifiedBy());
		productocamisetaDTO.setLastModifiedDate(productocamiseta.getLastModifiedDate());
		productocamisetaDTO.setColorId(productocamiseta.getColor() != null ? productocamiseta.getColor().getId() : null);
		productocamisetaDTO.setTallaId(productocamiseta.getTalla() != null ? productocamiseta.getTalla().getId() : null);
		productocamisetaDTO.setProductoId(productocamiseta.getProducto() != null ? productocamiseta.getProducto().getId() : null);
		return productocamisetaDTO;
	}

}
