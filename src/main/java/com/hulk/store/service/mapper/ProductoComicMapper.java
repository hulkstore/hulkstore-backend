package com.hulk.store.service.mapper;

import com.hulk.store.domain.ProductoComic;
import com.hulk.store.dto.ProductoComicDTO;
import org.springframework.stereotype.Service;
import com.hulk.store.domain.Producto;

@Service
public class ProductoComicMapper {

	public ProductoComic toEntity(ProductoComicDTO productocomicDTO) {
		ProductoComic productocomic = new ProductoComic();

		productocomic.setId(productocomicDTO.getId());
		productocomic.setTitulo(productocomicDTO.getTitulo());
		productocomic.setGenero(productocomicDTO.getGenero());
		productocomic.setVolumen(productocomicDTO.getVolumen());
		productocomic.setAnioLanzamiento(productocomicDTO.getAnioLanzamiento());
		productocomic.setIdioma(productocomicDTO.getIdioma());
		productocomic.setCreatedBy(productocomicDTO.getCreatedBy());
		productocomic.setCreatedDate(productocomicDTO.getCreatedDate());
		productocomic.setLastModifiedBy(productocomicDTO.getLastModifiedBy());
		productocomic.setLastModifiedDate(productocomicDTO.getLastModifiedDate());
		if (productocomicDTO.getProductoId() != null) {
			Producto producto = new Producto();
			producto.setId(productocomicDTO.getProductoId());
			productocomic.setProducto(producto);
		}
		return productocomic;
	}

	public ProductoComicDTO toDTO(ProductoComic productocomic) {
		ProductoComicDTO productocomicDTO = new ProductoComicDTO();

		productocomicDTO.setId(productocomic.getId());
		productocomicDTO.setTitulo(productocomic.getTitulo());
		productocomicDTO.setGenero(productocomic.getGenero());
		productocomicDTO.setVolumen(productocomic.getVolumen());
		productocomicDTO.setAnioLanzamiento(productocomic.getAnioLanzamiento());
		productocomicDTO.setIdioma(productocomic.getIdioma());
		productocomicDTO.setCreatedBy(productocomic.getCreatedBy());
		productocomicDTO.setCreatedDate(productocomic.getCreatedDate());
		productocomicDTO.setLastModifiedBy(productocomic.getLastModifiedBy());
		productocomicDTO.setLastModifiedDate(productocomic.getLastModifiedDate());
		productocomicDTO.setProductoId(productocomic.getProducto() != null ? productocomic.getProducto().getId() : null);
		return productocomicDTO;
	}

}
