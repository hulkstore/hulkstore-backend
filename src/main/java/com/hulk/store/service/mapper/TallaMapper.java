package com.hulk.store.service.mapper;

import org.springframework.stereotype.Service;

import com.hulk.store.domain.Talla;
import com.hulk.store.dto.TallaDTO;

@Service
public class TallaMapper {

	public Talla toEntity(TallaDTO tallaDTO) {
		Talla talla = new Talla();
		
		talla.setId(tallaDTO.getId());
		talla.setTalla(tallaDTO.getTalla());
		talla.setCreatedBy(tallaDTO.getCreatedBy());
		talla.setCreatedDate(tallaDTO.getCreatedDate());
		talla.setLastModifiedBy(tallaDTO.getLastModifiedBy());
		talla.setLastModifiedDate(tallaDTO.getLastModifiedDate());
		return talla;
	}

	public TallaDTO toDTO(Talla talla) {
		TallaDTO tallaDTO = new TallaDTO();
		
		tallaDTO.setId(talla.getId());
		tallaDTO.setTalla(talla.getTalla());
		tallaDTO.setCreatedBy(talla.getCreatedBy());
		tallaDTO.setCreatedDate(talla.getCreatedDate());
		tallaDTO.setLastModifiedBy(talla.getLastModifiedBy());
		tallaDTO.setLastModifiedDate(talla.getLastModifiedDate());
		return tallaDTO;
	}

}
