package com.hulk.store.service;

import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Talla;
import com.hulk.store.dto.TallaDTO;
import com.hulk.store.repository.TallaRepository;
import com.hulk.store.service.mapper.TallaMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class TallaService {

	private final TallaRepository tallaRepository;

	private final TallaMapper tallaMapper;

	public TallaService(TallaRepository tallaRepository, TallaMapper tallaMapper) {
		this.tallaRepository = tallaRepository;
		this.tallaMapper = tallaMapper;
	}

	public TallaDTO save(TallaDTO tallaDTO) {
		Talla talla = this.tallaMapper.toEntity(tallaDTO);
		this.audit(talla);
		tallaDTO = this.tallaMapper.toDTO(this.tallaRepository.save(talla));
		return tallaDTO;
	}

	public Optional<TallaDTO> findOne(Long tallaId) {
		return this.tallaRepository.findById(tallaId).map(t -> this.tallaMapper.toDTO(t));
	}

	public Page<TallaDTO> findAll(Pageable pageable) {
		return this.tallaRepository.findAll(pageable).map(t -> this.tallaMapper.toDTO(t));
	}

	public void deleteOne(Long tallaId) {
		this.tallaRepository.deleteById(tallaId);
	}

	public void audit(Talla talla) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (talla.getCreatedBy() == null) {
			talla.setCreatedBy(currentUser);
			talla.setCreatedDate(Instant.now());
		} else {
			talla.setLastModifiedBy(currentUser);
			talla.setLastModifiedDate(Instant.now());
		}
	}

	@Transactional(readOnly = true)
	public Page<TallaDTO> searchByKeyword(String keyword, Pageable pageable) {
		String query = "%" + keyword + "%";
		return this.tallaRepository.search(query, pageable).map(t -> this.tallaMapper.toDTO(t));
	}
}