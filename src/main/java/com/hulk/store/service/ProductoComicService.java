package com.hulk.store.service;

import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.hulk.store.domain.ProductoComic;
import com.hulk.store.dto.ProductoComicDTO;
import com.hulk.store.repository.ProductoComicRepository;
import com.hulk.store.service.mapper.ProductoComicMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class ProductoComicService {

	private final ProductoComicRepository productocomicRepository;

	private final ProductoComicMapper productoComicMapper;

	public ProductoComicService(ProductoComicRepository productocomicRepository,
			ProductoComicMapper productoComicMapper) {
		this.productocomicRepository = productocomicRepository;
		this.productoComicMapper = productoComicMapper;
	}

	public ProductoComicDTO save(ProductoComicDTO productocomicDTO) {
		ProductoComic productocomic = this.productoComicMapper.toEntity(productocomicDTO);
		this.audit(productocomic);
		productocomicDTO = this.productoComicMapper.toDTO(this.productocomicRepository.save(productocomic));
		return productocomicDTO;
	}

	public Optional<ProductoComicDTO> findOne(Long productocomicId) {
		return this.productocomicRepository.findById(productocomicId).map(t -> this.productoComicMapper.toDTO(t));
	}

	public Page<ProductoComicDTO> findAll(Pageable pageable) {
		return this.productocomicRepository.findAll(pageable).map(t -> this.productoComicMapper.toDTO(t));
	}

	public void deleteOne(Long productocomicId) {
		this.productocomicRepository.deleteById(productocomicId);
	}

	public void audit(ProductoComic productoComic) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (productoComic.getCreatedBy() == null) {
			productoComic.setCreatedBy(currentUser);
			productoComic.setCreatedDate(Instant.now());
		} else {
			productoComic.setLastModifiedBy(currentUser);
			productoComic.setLastModifiedDate(Instant.now());
		}
	}
}