package com.hulk.store.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.hulk.store.domain.InvDetalleEntrada;
import com.hulk.store.dto.InvDetalleEntradaDTO;
import com.hulk.store.repository.InvDetalleEntradaRepository;
import com.hulk.store.service.mapper.InvDetalleEntradaMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;

@Service
public class InvDetalleEntradaService {

	private final InvDetalleEntradaRepository invdetalleentradaRepository;

	private final InvDetalleEntradaMapper invDetalleEntradaMapper;

	public InvDetalleEntradaService(InvDetalleEntradaRepository invdetalleentradaRepository,
			InvDetalleEntradaMapper invDetalleEntradaMapper) {
		this.invdetalleentradaRepository = invdetalleentradaRepository;
		this.invDetalleEntradaMapper = invDetalleEntradaMapper;
	}

	public InvDetalleEntradaDTO save(InvDetalleEntradaDTO invdetalleentradaDTO) {
		InvDetalleEntrada invdetalleentrada = this.invDetalleEntradaMapper.toEntity(invdetalleentradaDTO);
		this.audit(invdetalleentrada);
		invdetalleentradaDTO = this.invDetalleEntradaMapper.toDTO(this.invdetalleentradaRepository.save(invdetalleentrada));
		return invdetalleentradaDTO;
	}

	public Optional<InvDetalleEntradaDTO> findOne(Long invdetalleentradaId) {
		return this.invdetalleentradaRepository.findById(invdetalleentradaId)
				.map(t -> this.invDetalleEntradaMapper.toDTO(t));
	}

	public Page<InvDetalleEntradaDTO> findAll(Pageable pageable) {
		return this.invdetalleentradaRepository.findAll(pageable).map(t -> this.invDetalleEntradaMapper.toDTO(t));
	}

	public void deleteOne(Long invdetalleentradaId) {
		this.invdetalleentradaRepository.deleteById(invdetalleentradaId);
	}

	public void audit(InvDetalleEntrada invDetalleEntrada) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (invDetalleEntrada.getCreatedBy() == null) {
			invDetalleEntrada.setCreatedBy(currentUser);
			invDetalleEntrada.setCreatedDate(Instant.now());
		} else {
			invDetalleEntrada.setLastModifiedBy(currentUser);
			invDetalleEntrada.setLastModifiedDate(Instant.now());
		}
	}
}