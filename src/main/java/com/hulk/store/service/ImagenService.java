package com.hulk.store.service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Imagen;
import com.hulk.store.dto.ImagenDTO;
import com.hulk.store.repository.ImagenRepository;
import com.hulk.store.repository.ProductoRepository;
import com.hulk.store.service.mapper.ImagenMapper;

@Service
public class ImagenService {

	private final ImagenRepository imagenRepository;

	private final ImagenMapper imagenMapper;

	private final ProductoRepository productoRepository;

	public ImagenService (ImagenRepository imagenRepository, ImagenMapper imagenMapper, ProductoRepository productoRepository) {
		this.imagenRepository = imagenRepository;
		this.imagenMapper = imagenMapper;
		this.productoRepository = productoRepository;
	}

	public ImagenDTO save(ImagenDTO imagenDTO) {
		Imagen imagen = this.imagenMapper.toEntity(imagenDTO);
		imagen.setProducto(productoRepository.findById(imagenDTO.getProductoId()).get());
		this.audit(imagen);
		imagenDTO = this.imagenMapper.toDTO(this.imagenRepository.save(imagen));
		return imagenDTO;
	}

	public Optional<ImagenDTO> findOne(Long imagenId) {
		return this.imagenRepository.findById(imagenId).map(t -> this.imagenMapper.toDTO(t));
	}

	public List<ImagenDTO> findAll() {
		return this.imagenRepository.findAll().stream().map(t -> this.imagenMapper.toDTO(t)).collect(Collectors.toList());
	}

	public void deleteOne(Long imagenId) {
		this.imagenRepository.deleteById(imagenId);
	}

    @Transactional(readOnly = true)
    public Page<ImagenDTO> findAllImagesByProductId(Long id, Pageable pageable) {
        return imagenRepository.findAllByProductoId(id, pageable).map(t -> this.imagenMapper.toDTO(t));
    }

	public void audit(Imagen imagen) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (imagen.getCreatedBy() == null) {
			imagen.setCreatedBy(currentUser);
			imagen.setCreatedDate(Instant.now());
		} else {
			imagen.setLastModifiedBy(currentUser);
			imagen.setLastModifiedDate(Instant.now());
		}
	}

}