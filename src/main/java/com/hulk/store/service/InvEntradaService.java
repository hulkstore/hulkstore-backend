package com.hulk.store.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.hulk.store.domain.InvEntrada;
import com.hulk.store.dto.InvEntradaDTO;
import com.hulk.store.repository.InvEntradaRepository;
import com.hulk.store.service.mapper.InvEntradaMapper;
import com.hulk.store.web.rest.vm.EntradaVM;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;
import com.hulk.store.domain.InvDetalleEntrada;
import com.hulk.store.service.InvDetalleEntradaService;
import com.hulk.store.repository.InvDetalleEntradaRepository;

@Service
public class InvEntradaService {

	private final InvEntradaRepository inventradaRepository;

	private final InvEntradaMapper invEntradaMapper;

	private final InvDetalleEntradaRepository invDetalleEntradaRepository;

	public InvEntradaService(InvEntradaRepository inventradaRepository, InvEntradaMapper invEntradaMapper,
			InvDetalleEntradaRepository invDetalleEntradaRepository) {
		this.inventradaRepository = inventradaRepository;
		this.invEntradaMapper = invEntradaMapper;
		this.invDetalleEntradaRepository = invDetalleEntradaRepository;
	}

	public InvEntradaDTO save(InvEntradaDTO inventradaDTO) {
		InvEntrada inventrada = this.invEntradaMapper.toEntity(inventradaDTO);
		this.audit(inventrada);
		inventradaDTO = this.invEntradaMapper.toDTO(this.inventradaRepository.save(inventrada));
		return inventradaDTO;
	}

	public Optional<InvEntradaDTO> findOne(Long inventradaId) {
		return this.inventradaRepository.findById(inventradaId).map(t -> this.invEntradaMapper.toDTO(t));
	}

	public Page<InvEntradaDTO> findAll(Pageable pageable) {
		return this.inventradaRepository.findAll(pageable).map(t -> this.invEntradaMapper.toDTO(t));
	}

	public void deleteOne(Long inventradaId) {
		this.inventradaRepository.deleteById(inventradaId);
	}

	public void audit(InvEntrada invEntrada) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (invEntrada.getCreatedBy() == null) {
			invEntrada.setCreatedBy(currentUser);
			invEntrada.setCreatedDate(Instant.now());
		} else {
			invEntrada.setLastModifiedBy(currentUser);
			invEntrada.setLastModifiedDate(Instant.now());
		}
	}

	public Optional<EntradaVM> findOneInvEntrada(Long id) {
		Optional<EntradaVM> entradaVM = inventradaRepository.findById(id)
				.map(t -> EntradaVM.parse(t, this.invDetalleEntradaRepository.findAllInvDetalleEntradaByEntradaId(id)));

		return entradaVM;
	}

	public Page<EntradaVM> findAllInvEntrada(Pageable pageable) {
		Page<EntradaVM> response = inventradaRepository.findAll(pageable)
				.map(invEntrada -> EntradaVM.parse(invEntrada,
						this.invDetalleEntradaRepository.findAllInvDetalleEntradaByEntradaId(invEntrada.getId())));
		return response;
	}
}