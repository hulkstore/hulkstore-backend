package com.hulk.store.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import com.hulk.store.domain.CategoriaProducto;
import com.hulk.store.dto.CategoriaProductoDTO;
import com.hulk.store.repository.CategoriaProductoRepository;
import com.hulk.store.service.mapper.CategoriaProductoMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;

@Service
public class CategoriaProductoService {

	private final CategoriaProductoRepository categoriaproductoRepository;

	private final CategoriaProductoMapper categoriaProductoMapper;

	public CategoriaProductoService(CategoriaProductoRepository categoriaproductoRepository,
			CategoriaProductoMapper categoriaProductoMapper) {
		this.categoriaproductoRepository = categoriaproductoRepository;
		this.categoriaProductoMapper = categoriaProductoMapper;
	}

	public CategoriaProductoDTO save(CategoriaProductoDTO categoriaproductoDTO) {
		CategoriaProducto categoriaproducto = this.categoriaProductoMapper.toEntity(categoriaproductoDTO);
		this.audit(categoriaproducto);
		categoriaproductoDTO = this.categoriaProductoMapper.toDTO(this.categoriaproductoRepository.save(categoriaproducto));
		return categoriaproductoDTO;
	}

	public Optional<CategoriaProductoDTO> findOne(Long categoriaproductoId) {
		return this.categoriaproductoRepository.findById(categoriaproductoId)
				.map(t -> this.categoriaProductoMapper.toDTO(t));
	}

	public Page<CategoriaProductoDTO> findAll(Pageable pageable) {
		return this.categoriaproductoRepository.findAll(pageable).map(t -> this.categoriaProductoMapper.toDTO(t));
	}

	public void deleteOne(Long categoriaproductoId) {
		this.categoriaproductoRepository.deleteById(categoriaproductoId);
	}

	public void audit(CategoriaProducto categoriaProducto) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (categoriaProducto.getCreatedBy() == null) {
			categoriaProducto.setCreatedBy(currentUser);
			categoriaProducto.setCreatedDate(Instant.now());
		} else {
			categoriaProducto.setLastModifiedBy(currentUser);
			categoriaProducto.setLastModifiedDate(Instant.now());
		}
	}

	@Transactional(readOnly = true)
	public Page<CategoriaProductoDTO> searchByKeyword(String keyword, Pageable pageable) {
		String query = "%" + keyword + "%";
		return this.categoriaproductoRepository.search(query, pageable).map(t -> this.categoriaProductoMapper.toDTO(t));
	}
}