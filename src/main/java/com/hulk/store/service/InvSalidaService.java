package com.hulk.store.service;

import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.hulk.store.domain.InvSalida;
import com.hulk.store.dto.InvSalidaDTO;
import com.hulk.store.repository.InvSalidaRepository;
import com.hulk.store.service.mapper.InvSalidaMapper;

@Service
public class InvSalidaService {

	private final InvSalidaRepository invsalidaRepository;

	private final InvSalidaMapper invSalidaMapper;

	public InvSalidaService(InvSalidaRepository invsalidaRepository, InvSalidaMapper invSalidaMapper) {
		this.invsalidaRepository = invsalidaRepository;
		this.invSalidaMapper = invSalidaMapper;
	}

	public InvSalidaDTO save(InvSalidaDTO invsalidaDTO) {
		InvSalida invsalida = this.invSalidaMapper.toEntity(invsalidaDTO);
		this.audit(invsalida);
		invsalidaDTO = this.invSalidaMapper.toDTO(this.invsalidaRepository.save(invsalida));
		return invsalidaDTO;
	}

	public Optional<InvSalidaDTO> findOne(Long invsalidaId) {
		return this.invsalidaRepository.findById(invsalidaId).map(t -> this.invSalidaMapper.toDTO(t));
	}

	public Page<InvSalidaDTO> findAll(Pageable pageable) {
		return this.invsalidaRepository.findAll(pageable).map(t -> this.invSalidaMapper.toDTO(t));
	}

	public void deleteOne(Long invsalidaId) {
		this.invsalidaRepository.deleteById(invsalidaId);
	}

	public void audit(InvSalida invSalida) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (invSalida.getCreatedBy() == null) {
			invSalida.setCreatedBy(currentUser);
			invSalida.setCreatedDate(Instant.now());
		} else {
			invSalida.setLastModifiedBy(currentUser);
			invSalida.setLastModifiedDate(Instant.now());
		}
	}
}