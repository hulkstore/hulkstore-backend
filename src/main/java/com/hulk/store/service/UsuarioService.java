package com.hulk.store.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.hulk.store.domain.Usuario;
import com.hulk.store.dto.UsuarioDTO;
import com.hulk.store.repository.UsuarioRepository;
import com.hulk.store.service.mapper.UsuarioMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;

@Service
public class UsuarioService {

	private final UsuarioRepository usuarioRepository;

	private final UsuarioMapper usuarioMapper;

	public UsuarioService(UsuarioRepository usuarioRepository, UsuarioMapper usuarioMapper) {
		this.usuarioRepository = usuarioRepository;
		this.usuarioMapper = usuarioMapper;
	}

	public UsuarioDTO save(UsuarioDTO usuarioDTO) {
		Usuario usuario = this.usuarioMapper.toEntity(usuarioDTO);
		this.audit(usuario);
		usuarioDTO = this.usuarioMapper.toDTO(this.usuarioRepository.save(usuario));
		return usuarioDTO;
	}

	public Optional<UsuarioDTO> findOne(Long usuarioId) {
		return this.usuarioRepository.findById(usuarioId).map(t -> this.usuarioMapper.toDTO(t));
	}

	public Page<UsuarioDTO> findAll(Pageable pageable) {
		return this.usuarioRepository.findAll(pageable).map(t -> this.usuarioMapper.toDTO(t));
	}

	public void deleteOne(Long usuarioId) {
		this.usuarioRepository.deleteById(usuarioId);
	}

	public void audit(Usuario usuario) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (usuario.getCreatedBy() == null) {
			usuario.setCreatedBy(currentUser);
			usuario.setCreatedDate(Instant.now());
		} else {
			usuario.setLastModifiedBy(currentUser);
			usuario.setLastModifiedDate(Instant.now());
		}
	}
}