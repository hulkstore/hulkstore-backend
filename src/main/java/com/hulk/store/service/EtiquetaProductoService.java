package com.hulk.store.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.EtiquetaProducto;
import com.hulk.store.dto.EtiquetaProductoDTO;
import com.hulk.store.repository.EtiquetaProductoRepository;
import com.hulk.store.service.mapper.EtiquetaProductoMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;

@Service
public class EtiquetaProductoService {

	private final EtiquetaProductoRepository etiquetaproductoRepository;

	private final EtiquetaProductoMapper etiquetaProductoMapper;

	public EtiquetaProductoService(EtiquetaProductoRepository etiquetaproductoRepository,
			EtiquetaProductoMapper etiquetaProductoMapper) {
		this.etiquetaproductoRepository = etiquetaproductoRepository;
		this.etiquetaProductoMapper = etiquetaProductoMapper;
	}

	public EtiquetaProductoDTO save(EtiquetaProductoDTO etiquetaproductoDTO) {
		EtiquetaProducto etiquetaproducto = this.etiquetaProductoMapper.toEntity(etiquetaproductoDTO);
		this.audit(etiquetaproducto);
		etiquetaproductoDTO = this.etiquetaProductoMapper.toDTO(this.etiquetaproductoRepository.save(etiquetaproducto));
		return etiquetaproductoDTO;
	}

	public Optional<EtiquetaProductoDTO> findOne(Long etiquetaproductoId) {
		return this.etiquetaproductoRepository.findById(etiquetaproductoId).map(t -> this.etiquetaProductoMapper.toDTO(t));
	}

	public Page<EtiquetaProductoDTO> findAll(Pageable pageable) {
		return this.etiquetaproductoRepository.findAll(pageable).map(t -> this.etiquetaProductoMapper.toDTO(t));
	}

	public void deleteOne(Long etiquetaproductoId) {
		this.etiquetaproductoRepository.deleteById(etiquetaproductoId);
	}

	public void audit(EtiquetaProducto etiquetaProducto) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (etiquetaProducto.getCreatedBy() == null) {
			etiquetaProducto.setCreatedBy(currentUser);
			etiquetaProducto.setCreatedDate(Instant.now());
		} else {
			etiquetaProducto.setLastModifiedBy(currentUser);
			etiquetaProducto.setLastModifiedDate(Instant.now());
		}
	}

	@Transactional(readOnly = true)
	public Page<EtiquetaProductoDTO> searchByKeyword(String keyword, Pageable pageable) {
		String query = "%" + keyword + "%";
		return this.etiquetaproductoRepository.search(query, pageable).map(t -> this.etiquetaProductoMapper.toDTO(t));
	}
}