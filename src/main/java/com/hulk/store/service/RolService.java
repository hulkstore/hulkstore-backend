package com.hulk.store.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import com.hulk.store.dto.RolDTO;
import com.hulk.store.service.mapper.RolMapper;
import com.hulk.store.repository.RolRepository;

@Service
public class RolService {
  private final RolMapper rolMapper;
  private final RolRepository rolRepository;

  public RolService(RolMapper rolMapper, RolRepository rolRepository) {
    this.rolMapper = rolMapper;
    this.rolRepository = rolRepository;
  }

  public List<RolDTO> findAll() {
    return this.rolRepository.findAll().stream().map(t -> this.rolMapper.toDTO(t)).collect(Collectors.toList());
  }
}
