package com.hulk.store.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.hulk.store.domain.ProductoCamiseta;
import com.hulk.store.dto.ProductoCamisetaDTO;
import com.hulk.store.repository.ProductoCamisetaRepository;
import com.hulk.store.service.mapper.ProductoCamisetaMapper;

@Service
public class ProductoCamisetaService {

	private final ProductoCamisetaRepository productocamisetaRepository;

	private final ProductoCamisetaMapper productoCamisetaMapper;

	public ProductoCamisetaService(ProductoCamisetaRepository productocamisetaRepository,
			ProductoCamisetaMapper productoCamisetaMapper) {
		this.productocamisetaRepository = productocamisetaRepository;
		this.productoCamisetaMapper = productoCamisetaMapper;
	}

	public ProductoCamisetaDTO save(ProductoCamisetaDTO productocamisetaDTO) {
		ProductoCamiseta productocamiseta = this.productoCamisetaMapper.toEntity(productocamisetaDTO);
		this.audit(productocamiseta);
		productocamisetaDTO = this.productoCamisetaMapper.toDTO(this.productocamisetaRepository.save(productocamiseta));
		return productocamisetaDTO;
	}

	public Optional<ProductoCamisetaDTO> findOne(Long productocamisetaId) {
		return this.productocamisetaRepository.findById(productocamisetaId).map(t -> this.productoCamisetaMapper.toDTO(t));
	}

	public Page<ProductoCamisetaDTO> findAll(Pageable pageable) {
		return this.productocamisetaRepository.findAll(pageable).map(t -> this.productoCamisetaMapper.toDTO(t));
	}

	public void deleteOne(Long productocamisetaId) {
		this.productocamisetaRepository.deleteById(productocamisetaId);
	}

	public void audit(ProductoCamiseta productoCamiseta) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (productoCamiseta.getCreatedBy() == null) {
			productoCamiseta.setCreatedBy(currentUser);
			productoCamiseta.setCreatedDate(Instant.now());
		} else {
			productoCamiseta.setLastModifiedBy(currentUser);
			productoCamiseta.setLastModifiedDate(Instant.now());
		}
	}
}