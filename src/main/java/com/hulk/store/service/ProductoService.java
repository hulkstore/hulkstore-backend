package com.hulk.store.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Imagen;
import com.hulk.store.domain.Producto;
import com.hulk.store.domain.ProductoCamiseta;
import com.hulk.store.domain.ProductoComic;
import com.hulk.store.dto.CategoriaProductoDTO;
import com.hulk.store.dto.ProductoDTO;
import com.hulk.store.repository.ProductoRepository;
import com.hulk.store.service.mapper.ProductoMapper;
import com.hulk.store.web.rest.vm.PreProductoVM;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import com.hulk.store.service.CategoriaProductoService;
import com.hulk.store.repository.ProductoCamisetaRepository;
import com.hulk.store.repository.ProductoComicRepository;
import com.hulk.store.repository.ImagenRepository;

@Service
public class ProductoService {

	private final ProductoRepository productoRepository;

	private final ProductoMapper productoMapper;
	private final CategoriaProductoService categoriaProductoService;
	private final ProductoCamisetaRepository productoCamisetaRepository;
	private final ProductoComicRepository productoComicRepository;
	private final ImagenRepository imagenRepository;

	public ProductoService(ProductoRepository productoRepository, ProductoMapper productoMapper,
			CategoriaProductoService categoriaProductoService, ProductoCamisetaRepository productoCamisetaRepository,
			ProductoComicRepository productoComicRepository, ImagenRepository imagenRepository) {
		this.productoRepository = productoRepository;
		this.productoMapper = productoMapper;
		this.categoriaProductoService = categoriaProductoService;
		this.productoCamisetaRepository = productoCamisetaRepository;
		this.productoComicRepository = productoComicRepository;
		this.imagenRepository = imagenRepository;
	}

	public ProductoDTO save(ProductoDTO productoDTO) {
		Producto producto = this.productoMapper.toEntity(productoDTO);
		List<Imagen> im = this.imagenRepository.findAllByProductoId(productoDTO.getId());
		producto.setImagenes(im);
		this.audit(producto);
		productoDTO = this.productoMapper.toDTO(this.productoRepository.save(producto));
		return productoDTO;
	}

	public Optional<ProductoDTO> findOne(Long productoId) {
		return this.productoRepository.findById(productoId).map(t -> this.productoMapper.toDTO(t));
	}

	public Page<ProductoDTO> findAll(Pageable pageable) {
		return this.productoRepository.findAll(pageable).map(t -> this.productoMapper.toDTO(t));
	}

	public void deleteOne(Long productoId) {
		this.productoRepository.deleteById(productoId);
	}

	public void audit(Producto producto) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (producto.getCreatedBy() == null) {
			producto.setCreatedBy(currentUser);
			producto.setCreatedDate(Instant.now());
		} else {
			producto.setLastModifiedBy(currentUser);
			producto.setLastModifiedDate(Instant.now());
		}
	}

	public Optional<PreProductoVM> findOneProduct(Long productoId) {
		PreProductoVM aux = new PreProductoVM();
		Optional<PreProductoVM> preProductoVM = Optional.of(aux);
		Optional<Producto> producto = this.productoRepository.findById(productoId);
		if (producto.isPresent()) {
			preProductoVM.get().setId(producto.get().getId());
			preProductoVM.get().setNombre(producto.get().getNombre());
			preProductoVM.get().setDescripcion(producto.get().getDescripcion());
			preProductoVM.get().setCodigo(producto.get().getCodigo());
			preProductoVM.get().setCostoUnitarioCompra(producto.get().getCostoUnitarioCompra());
			preProductoVM.get().setPrecioUnitarioVenta(producto.get().getPrecioUnitarioVenta());
			preProductoVM.get().setCantidad(producto.get().getCantidad());
			preProductoVM.get().setEstado(producto.get().getEstado());
			preProductoVM.get().setCategoria(producto.get().getCategoria());
			preProductoVM.get().setEtiqueta(producto.get().getEtiqueta());
			preProductoVM.get()
					.setCategoriaId(producto.get().getCategoria() != null ? producto.get().getCategoria().getId() : null);
			preProductoVM.get()
					.setEtiquetaId(producto.get().getEtiqueta() != null ? producto.get().getEtiqueta().getId() : null);
			preProductoVM.get().setCreatedBy(producto.get().getCreatedBy());
			preProductoVM.get().setCreatedDate(producto.get().getCreatedDate());
			preProductoVM.get().setLastModifiedBy(producto.get().getLastModifiedBy());
			preProductoVM.get().setLastModifiedDate(producto.get().getLastModifiedDate());
			List<Imagen> images = this.imagenRepository.findAllByProductoId(productoId);
			if (images.size() != 0) {
				preProductoVM.get().setImageUrl(images.get(0).getUrl());
			}
			// Optional<CategoriaProductoDTO> categoriaProductoDTO =
			// categoriaProductoService.findOne(producto.get().getCategoria().getId());
			if (producto.get().getCategoria() != null) {
				if (producto.get().getCategoria().getNombre().equals("CAMISETA")) {
					Optional<ProductoCamiseta> productoCamiseta = productoCamisetaRepository.findOneByProductoId(productoId);
					if (productoCamiseta.isPresent()) {
						preProductoVM.get().setId(productoCamiseta.get().getId());
						preProductoVM.get().setSexo(productoCamiseta.get().getSexo());
						preProductoVM.get()
								.setTalla(productoCamiseta.get().getTalla() != null ? productoCamiseta.get().getTalla() : null);
						preProductoVM.get()
								.setColor(productoCamiseta.get().getColor() != null ? productoCamiseta.get().getColor() : null);
						preProductoVM.get().setTallaId(
								productoCamiseta.get().getTalla() != null ? productoCamiseta.get().getTalla().getId() : null);
						preProductoVM.get().setColorId(
								productoCamiseta.get().getColor() != null ? productoCamiseta.get().getColor().getId() : null);
					}
				} else if (producto.get().getCategoria().getNombre().equals("COMICS")) {
					Optional<ProductoComic> productoComic = productoComicRepository.findOneByProductoId(productoId);
					if (productoComic.isPresent()) {
						preProductoVM.get().setId(productoComic.get().getId());
						preProductoVM.get().setTitulo(productoComic.get().getTitulo());
						preProductoVM.get().setGenero(productoComic.get().getGenero());
						preProductoVM.get().setVolumen(productoComic.get().getVolumen());
						preProductoVM.get().setAnioLanzamiento(productoComic.get().getAnioLanzamiento());
						preProductoVM.get().setIdioma(productoComic.get().getIdioma());
					}
				}
			}
		}
		return preProductoVM;
	}

	public Page<PreProductoVM> findAllProducts(Pageable pageable) {
		Page<Producto> productoPage = this.productoRepository.findAll(pageable);
		List<PreProductoVM> preProductoVMList = new ArrayList<PreProductoVM>();
		productoPage.forEach((Producto producto) -> {
			Optional<Producto> productoAux = this.productoRepository.findById(producto.getId());
			PreProductoVM preProductoVM = new PreProductoVM();
			preProductoVM.setId(productoAux.get().getId());
			preProductoVM.setNombre(productoAux.get().getNombre());
			preProductoVM.setDescripcion(productoAux.get().getDescripcion());
			preProductoVM.setCodigo(productoAux.get().getCodigo());
			preProductoVM.setCostoUnitarioCompra(productoAux.get().getCostoUnitarioCompra());
			preProductoVM.setPrecioUnitarioVenta(productoAux.get().getPrecioUnitarioVenta());
			preProductoVM.setCantidad(productoAux.get().getCantidad());
			preProductoVM.setEstado(productoAux.get().getEstado());
			preProductoVM.setCategoria(productoAux.get().getCategoria());
			preProductoVM.setEtiqueta(productoAux.get().getEtiqueta());
			preProductoVM.setCreatedBy(productoAux.get().getCreatedBy());
			preProductoVM.setCreatedDate(productoAux.get().getCreatedDate());
			preProductoVM.setLastModifiedBy(productoAux.get().getLastModifiedBy());
			preProductoVM.setLastModifiedDate(productoAux.get().getLastModifiedDate());
			List<Imagen> images = this.imagenRepository.findAllByProductoId(productoAux.get().getId());
			if (images.size() != 0) {
				preProductoVM.setImageUrl(images.get(0).getUrl());
			}
			Optional<CategoriaProductoDTO> categoriaProductoDTO = categoriaProductoService
					.findOne(productoAux.get().getCategoria().getId());
			if (categoriaProductoDTO.isPresent()) {
				if (categoriaProductoDTO.get().getNombre().equals("CAMISETA")) {
					Optional<ProductoCamiseta> productoCamiseta = productoCamisetaRepository
							.findOneByProductoId(productoAux.get().getId());

					if (productoCamiseta.isPresent()) {
						preProductoVM.setSexo(productoCamiseta.get().getSexo());
						preProductoVM.setTalla(productoCamiseta.get().getTalla());
						preProductoVM.setColor(productoCamiseta.get().getColor());
					}
				} else if (categoriaProductoDTO.get().getNombre().equals("COMICS")) {
					Optional<ProductoComic> productoComic = productoComicRepository
							.findOneByProductoId(productoAux.get().getId());
					if (productoComic.isPresent()) {
						preProductoVM.setTitulo(productoComic.get().getTitulo());
						preProductoVM.setGenero(productoComic.get().getGenero());
						preProductoVM.setVolumen(productoComic.get().getVolumen());
						preProductoVM.setAnioLanzamiento(productoComic.get().getAnioLanzamiento());
						preProductoVM.setIdioma(productoComic.get().getIdioma());
					}
				}
			}
			preProductoVMList.add(preProductoVM);
		});
		Page<PreProductoVM> preProductoVMPage = new PageImpl<PreProductoVM>(preProductoVMList, pageable,
				preProductoVMList.size());
		return preProductoVMPage;
	}

	@Transactional(readOnly = true)
	public Page<ProductoDTO> searchByKeyword(String keyword, Pageable pageable) {
		String query = "%" + keyword + "%";
		return this.productoRepository.search(query, pageable).map(t -> this.productoMapper.toDTO(t));
	}

	@Transactional(readOnly = true)
	public String generateCodeByProduct(String productName) {
		String sTexto = productName;
		String sPalabra = "";
		StringTokenizer stPalabras = new StringTokenizer(sTexto);
		while (stPalabras.hasMoreTokens()) {
			sPalabra += stPalabras.nextToken().substring(0, 1);
		}
		sPalabra = sPalabra.toUpperCase();
		List<Producto> productos = this.productoRepository.findAllByCodigoContainingIgnoreCase(sPalabra);
		Collections.sort(productos, new Comparator<Producto>() {
			public int compare(Producto o1, Producto o2) {
				return o2.getCodigo().compareTo(o1.getCodigo());
			}
		});

		if (productos.size() > 0) {
			String data = productos.get(0).getCodigo().substring(productos.get(0).getCodigo().length() - 1,
					productos.get(0).getCodigo().length());
			Integer num = Integer.parseInt(data) + 1;
			sPalabra = sPalabra + "-" + num.toString();
		} else {
			sPalabra = sPalabra + "-1";
		}
		return sPalabra;
	}

}