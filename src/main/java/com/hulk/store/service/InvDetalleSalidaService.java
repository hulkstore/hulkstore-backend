package com.hulk.store.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.hulk.store.domain.InvDetalleSalida;
import com.hulk.store.dto.InvDetalleSalidaDTO;
import com.hulk.store.repository.InvDetalleSalidaRepository;
import com.hulk.store.service.mapper.InvDetalleSalidaMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import java.time.Instant;

@Service
public class InvDetalleSalidaService {

	private final InvDetalleSalidaRepository invdetallesalidaRepository;

	private final InvDetalleSalidaMapper invDetalleSalidaMapper;

	public InvDetalleSalidaService(InvDetalleSalidaRepository invdetallesalidaRepository,
			InvDetalleSalidaMapper invDetalleSalidaMapper) {
		this.invdetallesalidaRepository = invdetallesalidaRepository;
		this.invDetalleSalidaMapper = invDetalleSalidaMapper;
	}

	public InvDetalleSalidaDTO save(InvDetalleSalidaDTO invdetallesalidaDTO) {
		InvDetalleSalida invdetallesalida = this.invDetalleSalidaMapper.toEntity(invdetallesalidaDTO);
		this.audit(invdetallesalida);
		invdetallesalidaDTO = this.invDetalleSalidaMapper.toDTO(this.invdetallesalidaRepository.save(invdetallesalida));
		return invdetallesalidaDTO;
	}

	public Optional<InvDetalleSalidaDTO> findOne(Long invdetallesalidaId) {
		return this.invdetallesalidaRepository.findById(invdetallesalidaId).map(t -> this.invDetalleSalidaMapper.toDTO(t));
	}

	public Page<InvDetalleSalidaDTO> findAll(Pageable pageable) {
		return this.invdetallesalidaRepository.findAll(pageable).map(t -> this.invDetalleSalidaMapper.toDTO(t));
	}

	public void deleteOne(Long invdetallesalidaId) {
		this.invdetallesalidaRepository.deleteById(invdetallesalidaId);
	}

	public void audit(InvDetalleSalida invDetalleSalida) {
		String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
		if (invDetalleSalida.getCreatedBy() == null) {
			invDetalleSalida.setCreatedBy(currentUser);
			invDetalleSalida.setCreatedDate(Instant.now());
		} else {
			invDetalleSalida.setLastModifiedBy(currentUser);
			invDetalleSalida.setLastModifiedDate(Instant.now());
		}
	}
}