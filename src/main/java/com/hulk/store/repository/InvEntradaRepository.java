package com.hulk.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hulk.store.domain.InvEntrada;

public interface InvEntradaRepository extends JpaRepository<InvEntrada, Long> {

}
