package com.hulk.store.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hulk.store.domain.Imagen;

public interface ImagenRepository extends JpaRepository<Imagen, Long> {
  @Query(value = "select i from Imagen i where i.producto.id = :id")
  List<Imagen> findAllByProductoId(@Param("id") Long productoId);
  Page<Imagen> findAllByProductoId(Long productoId, Pageable pageable);

}
