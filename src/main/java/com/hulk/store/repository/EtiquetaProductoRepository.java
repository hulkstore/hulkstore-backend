package com.hulk.store.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hulk.store.domain.EtiquetaProducto;

public interface EtiquetaProductoRepository extends JpaRepository<EtiquetaProducto, Long> {
  Optional<EtiquetaProducto> findOneByNombre(String nombre);

  String queryOne = "select distinct c.* " +
      "from etiquetaproducto c " +
      "where LOWER(c.nombre) like LOWER(:keyword) " +
      " or LOWER(c.descripcion) like LOWER(:keyword)";

  @Query(value = queryOne, nativeQuery = true)
  Page<EtiquetaProducto> search(@Param("keyword") String data, Pageable pageable);
}
