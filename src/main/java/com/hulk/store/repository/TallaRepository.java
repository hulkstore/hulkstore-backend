package com.hulk.store.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hulk.store.domain.Talla;

public interface TallaRepository extends JpaRepository<Talla, Long> {

  Optional<Talla> findOneByTalla(String talla);

  String queryOne = "select distinct c.* " +
      "from talla c " +
      "where LOWER(c.talla) like LOWER(:keyword) ";

  @Query(value = queryOne, nativeQuery = true)
  Page<Talla> search(@Param("keyword") String data, Pageable pageable);

}
