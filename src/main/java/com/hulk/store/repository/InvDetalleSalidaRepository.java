package com.hulk.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hulk.store.domain.InvDetalleSalida;

public interface InvDetalleSalidaRepository extends JpaRepository<InvDetalleSalida, Long> {

}
