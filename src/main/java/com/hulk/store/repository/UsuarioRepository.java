package com.hulk.store.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hulk.store.domain.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findOneByEmailOrUsername(String email, String username);

    Optional<Usuario> findOneByEmail(String email);

    Optional<Usuario> findOneByUsername(String username);

}
