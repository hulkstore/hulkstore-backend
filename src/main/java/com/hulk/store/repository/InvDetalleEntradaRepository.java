package com.hulk.store.repository;

import org.springframework.data.repository.query.Param;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hulk.store.domain.InvDetalleEntrada;

public interface InvDetalleEntradaRepository extends JpaRepository<InvDetalleEntrada, Long> {

  @Query(value = "select de from InvDetalleEntrada de where de.invEntrada.id = :id")
  List<InvDetalleEntrada> findAllInvDetalleEntradaByEntradaId(@Param("id") Long id);
}
