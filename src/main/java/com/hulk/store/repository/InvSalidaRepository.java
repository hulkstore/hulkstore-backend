package com.hulk.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hulk.store.domain.InvSalida;

public interface InvSalidaRepository extends JpaRepository<InvSalida, Long> {

}
