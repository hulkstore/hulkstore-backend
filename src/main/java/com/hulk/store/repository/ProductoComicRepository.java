package com.hulk.store.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hulk.store.domain.ProductoComic;

public interface ProductoComicRepository extends JpaRepository<ProductoComic, Long> {

  @Query(value = "select p from ProductoComic p where p.producto.id = :id")
  Optional<ProductoComic> findOneByProductoId(@Param("id") Long productoId);
}
