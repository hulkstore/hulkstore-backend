package com.hulk.store.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hulk.store.domain.CategoriaProducto;
import com.hulk.store.domain.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Long> {

  String queryOne = "select distinct p.* " +
      "from producto p " +
      "where  LOWER(p.nombre) like LOWER(:keyword) " +
      " or CAST(p.cantidad AS VARCHAR) like :keyword " +
      " or LOWER(p.codigo) like LOWER(:keyword) " +
      " or CAST(p.costo_unitario_compra AS VARCHAR) like :keyword " +
      " or LOWER(p.descripcion) like LOWER(:keyword) " +
      " or CAST(p.precio_unitario_venta AS VARCHAR) like :keyword ";

    @Query(value = queryOne, nativeQuery = true)
    Page<Producto> search(@Param("keyword") String data, Pageable pageable);
    
    List<Producto> findAllByCodigoContainingIgnoreCase(String codigo);
    Optional<Producto> findOneByCodigo(String codigo);

}
