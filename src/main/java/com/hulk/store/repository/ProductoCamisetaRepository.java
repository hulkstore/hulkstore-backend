package com.hulk.store.repository;

import java.util.Optional;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hulk.store.domain.ProductoCamiseta;
import org.springframework.data.repository.query.Param;

public interface ProductoCamisetaRepository extends JpaRepository<ProductoCamiseta, Long> {

  @Query(value = "select p from ProductoCamiseta p where p.producto.id = :id")
  Optional<ProductoCamiseta> findOneByProductoId(@Param("id") Long productoId);

}
