package com.hulk.store.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hulk.store.domain.Color;
import java.util.Optional;

public interface ColorRepository extends JpaRepository<Color, Long> {

  Optional<Color> findOneByCodigo(String codigo);
  String queryOne = "select distinct c.* " +
                    "from color c " +
                    "where LOWER(c.nombre) like LOWER(:keyword) " +
                    "or LOWER(CAST(c.codigo as varchar)) like LOWER(:keyword)";
  @Query(value = queryOne, nativeQuery = true)
  Page<Color> search(@Param("keyword") String data, Pageable pageable);

}
