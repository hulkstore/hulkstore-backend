package com.hulk.store.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hulk.store.domain.CategoriaProducto;
import java.util.Optional;

public interface CategoriaProductoRepository extends JpaRepository<CategoriaProducto, Long> {

  Optional<CategoriaProducto> findOneByNombre(String nombre);

  String queryOne = "select distinct c.* " +
      "from categoriaproducto c " +
      "where LOWER(c.nombre) like LOWER(:keyword)";

  @Query(value = queryOne, nativeQuery = true)
  Page<CategoriaProducto> search(@Param("keyword") String data, Pageable pageable);
}
