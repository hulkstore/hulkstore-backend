package com.hulk.store.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hulk.store.domain.audit.Audit;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "usuario")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Access(AccessType.FIELD)
public class Usuario extends Audit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;
	@Column
	private String nombre;
	@Column
	private Long nroTarjeta;
	@NotNull
	@Size(min = 1, max = 50)
	@Column(length = 50, unique = true, nullable = false)
	private String username;
	@JsonIgnore
	@NotNull
	@Size(min = 3, max = 60)
	@Column(name = "password_hash", length = 60, nullable = false)
	private String password;
	@Column
	private String email;
	@Column
	private Boolean estado;
	@ManyToMany
	@JoinTable(name = "usuario_rol", joinColumns = {
			@JoinColumn(name = "usuario_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "rol_nombre", referencedColumnName = "nombre") })
	private Set<Rol> roles = new HashSet<>();

}
