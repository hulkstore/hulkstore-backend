package com.hulk.store.domain; 

import javax.persistence.*; 
import org.hibernate.annotations.Cache; 
import org.hibernate.annotations.CacheConcurrencyStrategy; 
import com.hulk.store.domain.audit.Audit;

import lombok.*; 

@EqualsAndHashCode(callSuper = false) 
@Entity 
@Getter 
@Setter 
@NoArgsConstructor 
@Table(name="color") 
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE) 
@Access(AccessType.FIELD) 
public class Color extends Audit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
	private Long id;
	@Column
	private String nombre;
	@Column
	private String codigo;

} 
