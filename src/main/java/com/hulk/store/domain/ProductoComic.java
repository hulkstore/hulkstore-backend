package com.hulk.store.domain;

import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.hulk.store.domain.audit.Audit;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "productocomic")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Access(AccessType.FIELD)
public class ProductoComic extends Audit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;
	@Column
	private String titulo;
	@Column
	private String genero;
	@Column
	private Integer volumen;
	@Column
	private Integer anioLanzamiento;
	@Column
	private String idioma;
	@OneToOne
	@JoinColumn(name = "producto_id", referencedColumnName = "id")
	private Producto producto;

}
