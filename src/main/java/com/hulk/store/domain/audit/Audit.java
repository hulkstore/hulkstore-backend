package com.hulk.store.domain.audit;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
 

@MappedSuperclass
@Getter
@Setter
public abstract class Audit {
    
    @CreatedBy
    @Column(name = "created_by", nullable = false, length = 50, updatable = false)
	private String createdBy;

	@Column(name = "created_date")
    @CreatedDate
	private Instant createdDate;

    @LastModifiedBy
    @Column(name = "last_modified_by", length = 50)
	private String lastModifiedBy;

	@Column(name = "last_modified_date")
    @LastModifiedDate
	private Instant lastModifiedDate;

}
