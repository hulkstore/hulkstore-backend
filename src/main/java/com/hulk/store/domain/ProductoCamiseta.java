package com.hulk.store.domain;

import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.hulk.store.domain.audit.Audit;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "productocamiseta")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Access(AccessType.FIELD)
public class ProductoCamiseta extends Audit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;
	@Column
	private String sexo;
	@ManyToOne
	@JoinColumn(name = "color_id", referencedColumnName = "id")
	private Color color;
	@ManyToOne
	@JoinColumn(name = "talla_id", referencedColumnName = "id")
	private Talla talla;
	@OneToOne
	@JoinColumn(name = "producto_id", referencedColumnName = "id")
	private Producto producto;

}
