package com.hulk.store.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.hulk.store.domain.audit.Audit;

import lombok.*; 

@EqualsAndHashCode(callSuper = false) 
@Entity 
@Getter 
@Setter 
@NoArgsConstructor 
@Table(name="rol") 
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE) 
@Access(AccessType.FIELD) 
public class Rol {

    @NotNull
    @Size(max = 50)
    @Id
    @Column(length = 50)
    private String nombre;

}
