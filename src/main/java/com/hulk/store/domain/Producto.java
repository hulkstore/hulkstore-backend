package com.hulk.store.domain;

import java.util.List;

import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hulk.store.domain.audit.Audit;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "producto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Access(AccessType.FIELD)
public class Producto extends Audit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;
	@Column
	private String nombre;
	@Column
	private String descripcion;
	@Column
	private String codigo;
	@Column
	private Double costoUnitarioCompra;
	@Column
	private Double precioUnitarioVenta;
	@Column
	private Integer cantidad;
	@Column
	private Boolean estado;
	@ManyToOne
	@JoinColumn(name = "categoria_id", referencedColumnName = "id")
	private CategoriaProducto categoria;
	@ManyToOne
	@JoinColumn(name = "etiqueta_id", referencedColumnName = "id")
	private EtiquetaProducto etiqueta;
	@OneToOne(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "producto")
    @JsonBackReference
    private ProductoCamiseta productoCamiseta; // mostrar ProductoCamiseta
	@OneToOne(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "producto")
    @JsonBackReference
    private ProductoComic productoComic; // mostrar ProductoComic
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "producto")
    @JsonBackReference
    private List<Imagen> imagenes; // Listar las imagenes

}
