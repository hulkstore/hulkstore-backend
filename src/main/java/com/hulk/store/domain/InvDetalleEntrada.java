package com.hulk.store.domain;

import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.hulk.store.domain.audit.Audit;

import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "invdetalleentrada")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Access(AccessType.FIELD)
public class InvDetalleEntrada extends Audit {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;
	@Column
	private Double costoUnitario;
	@Column
	private Integer cantidad;
	@Column
	private Double subTotal;
	@ManyToOne
	@JoinColumn(name = "producto_id", referencedColumnName = "id")
	private Producto producto;
	@ManyToOne
	@JoinColumn(name = "inventrada_id", referencedColumnName = "id")
	private InvEntrada invEntrada;
}
