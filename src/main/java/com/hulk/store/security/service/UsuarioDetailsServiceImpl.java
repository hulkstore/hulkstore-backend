package com.hulk.store.security.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Usuario;
import com.hulk.store.repository.UsuarioRepository;

@Service
public class UsuarioDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UsuarioRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Usuario> user;
		user = userRepository.findOneByEmailOrUsername(username, username);
		if (!user.isPresent()) {
			throw new UsernameNotFoundException("Usuario no encontrado con nombre : " + username);
		}
		if (!user.get().getEstado()) {
			throw new UsernameNotFoundException("El usuario esta desactivado con nombre : "+ username);
		}
		return UsuarioDetailsImpl.build(user.get());
	}

}