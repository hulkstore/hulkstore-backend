package com.hulk.store.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.InvDetalleEntradaDTO;
import com.hulk.store.service.InvDetalleEntradaService;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class InvDetalleEntradaResource {

	private final InvDetalleEntradaService invdetalleentradaService;

	public InvDetalleEntradaResource(InvDetalleEntradaService invdetalleentradaService) {
		this.invdetalleentradaService = invdetalleentradaService;
	}

	@PostMapping("/invdetalleentradas")
	public ResponseEntity<InvDetalleEntradaDTO> create(@RequestBody InvDetalleEntradaDTO invdetalleentradaDTO) {
		if (invdetalleentradaDTO.getId() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID null");
		}
		InvDetalleEntradaDTO invdetalleentradaDTO2 = this.invdetalleentradaService.save(invdetalleentradaDTO);
		return ResponseEntity.ok().body(invdetalleentradaDTO2);
	}

	@PutMapping("/invdetalleentradas")
	public ResponseEntity<InvDetalleEntradaDTO> update(@RequestBody InvDetalleEntradaDTO invdetalleentradaDTO) {
		Optional<InvDetalleEntradaDTO> existingInvDetalleEntrada = this.invdetalleentradaService
				.findOne(invdetalleentradaDTO.getId());
		if (existingInvDetalleEntrada.isPresent()
				&& (!existingInvDetalleEntrada.get().getId().equals(invdetalleentradaDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the InvDetalleEntrada already exists");
		}
		InvDetalleEntradaDTO invdetalleentradaDTO2 = this.invdetalleentradaService.save(invdetalleentradaDTO);
		return ResponseEntity.ok().body(invdetalleentradaDTO2);
	}

	@GetMapping("/invdetalleentradas/{id}")
	public ResponseEntity<InvDetalleEntradaDTO> findOne(@PathVariable Long id) {
		Optional<InvDetalleEntradaDTO> invdetalleentradaDTO2 = this.invdetalleentradaService.findOne(id);
		if (!invdetalleentradaDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the InvDetalleEntrada dosent exist");
		}
		return ResponseEntity.ok().body(invdetalleentradaDTO2.get());
	}

	@GetMapping("/invdetalleentradas")
	public ResponseEntity<List<InvDetalleEntradaDTO>> findAll(Pageable pageable) {
		Page<InvDetalleEntradaDTO> response = this.invdetalleentradaService.findAll(pageable);
		List<InvDetalleEntradaDTO> lista = response.getContent();
		return ResponseEntity.ok().body(lista);
	}

	@DeleteMapping("/invdetalleentradas/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.invdetalleentradaService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

}
