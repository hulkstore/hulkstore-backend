package com.hulk.store.web.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.TallaDTO;
import com.hulk.store.config.Constants;
import com.hulk.store.domain.Talla;
import com.hulk.store.service.TallaService;
import com.hulk.store.repository.TallaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class TallaResource {

	private final TallaService tallaService;
	private final TallaRepository tallaRepository;

	public TallaResource(TallaService tallaService, TallaRepository tallaRepository) {
		this.tallaService = tallaService;
		this.tallaRepository = tallaRepository;
	}

	@PostMapping("/tallas")
	public ResponseEntity<TallaDTO> create(@RequestBody TallaDTO tallaDTO) {
		if (tallaDTO.getId() != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID null");
		} else if (tallaRepository.findOneByTalla(tallaDTO.getTalla()).isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La talla ya esta registrada");
		}
		TallaDTO tallaDTO2 = this.tallaService.save(tallaDTO);
		return ResponseEntity.ok().body(tallaDTO2);
	}

	@PutMapping("/tallas")
	public ResponseEntity<TallaDTO> update(@RequestBody TallaDTO tallaDTO) {
		Optional<TallaDTO> existingTalla = this.tallaService.findOne(tallaDTO.getId());
		if (existingTalla.isPresent() && (!existingTalla.get().getId().equals(tallaDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the Talla already exists");
		}
		Optional<Talla> existingTalla2 = tallaRepository.findOneByTalla(tallaDTO.getTalla());
		if (existingTalla2.isPresent() && (!existingTalla2.get().getId().equals(tallaDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La talla ya esta registrada");
		}
		TallaDTO tallaDTO2 = this.tallaService.save(tallaDTO);
		return ResponseEntity.ok().body(tallaDTO2);
	}

	@GetMapping("/tallas/{id}")
	public ResponseEntity<TallaDTO> findOne(@PathVariable Long id) {
		Optional<TallaDTO> tallaDTO2 = this.tallaService.findOne(id);
		if (!tallaDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the Talla dosent exist");
		}
		return ResponseEntity.ok().body(tallaDTO2.get());
	}

	@GetMapping("/tallas")
	public ResponseEntity<List<TallaDTO>> findAll(Pageable pageable) {
		Page<TallaDTO> page = this.tallaService.findAll(pageable);
		HttpHeaders response = new HttpHeaders();
		response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
	}

	@DeleteMapping("/tallas/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.tallaService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

	@GetMapping("/tallas/search")
	public ResponseEntity<List<TallaDTO>> searchByKeyword(@RequestParam String keyword,
			Pageable pageable) {
		Page<TallaDTO> page = this.tallaService.searchByKeyword(keyword, pageable);
		HttpHeaders response = new HttpHeaders();
		response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
	}
}
