package com.hulk.store.web.rest;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.hulk.store.config.Constants;
import com.hulk.store.domain.CategoriaProducto;
import com.hulk.store.dto.CategoriaProductoDTO;
import com.hulk.store.service.CategoriaProductoService;
import com.hulk.store.repository.CategoriaProductoRepository;
import com.hulk.store.config.Constants;
import org.springframework.http.HttpHeaders;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class CategoriaProductoResource {

	private final CategoriaProductoService categoriaproductoService;
	private final CategoriaProductoRepository categoriaProductoRepository;

	public CategoriaProductoResource(CategoriaProductoService categoriaproductoService,
			CategoriaProductoRepository categoriaProductoRepository) {
		this.categoriaproductoService = categoriaproductoService;
		this.categoriaProductoRepository = categoriaProductoRepository;
	}

	@PostMapping("/categoriaproductos")
	public ResponseEntity<CategoriaProductoDTO> create(@RequestBody CategoriaProductoDTO categoriaproductoDTO) {
		if (categoriaproductoDTO.getId() != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID exist");
		} else if (categoriaProductoRepository.findOneByNombre(categoriaproductoDTO.getNombre()).isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La categoria esta registrada");
		}
		CategoriaProductoDTO categoriaproductoDTO2 = this.categoriaproductoService.save(categoriaproductoDTO);
		return ResponseEntity.ok().body(categoriaproductoDTO2);
	}

	@PutMapping("/categoriaproductos")
	public ResponseEntity<CategoriaProductoDTO> update(@RequestBody CategoriaProductoDTO categoriaproductoDTO) {
		Optional<CategoriaProductoDTO> existingCategoriaProducto = this.categoriaproductoService
				.findOne(categoriaproductoDTO.getId());
		if (existingCategoriaProducto.isPresent()
				&& (!existingCategoriaProducto.get().getId().equals(categoriaproductoDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the CategoriaProducto already exists");
		}
		Optional<CategoriaProducto> existingCategoriaProducto2 = this.categoriaProductoRepository
				.findOneByNombre(categoriaproductoDTO.getNombre());

		if (existingCategoriaProducto2.isPresent()
				&& (!existingCategoriaProducto2.get().getId().equals(categoriaproductoDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Categoria already exists");
		}
		CategoriaProductoDTO categoriaproductoDTO2 = this.categoriaproductoService.save(categoriaproductoDTO);
		return ResponseEntity.ok().body(categoriaproductoDTO2);
	}

	@GetMapping("/categoriaproductos/{id}")
	public ResponseEntity<CategoriaProductoDTO> findOne(@PathVariable Long id) {
		Optional<CategoriaProductoDTO> categoriaproductoDTO2 = this.categoriaproductoService.findOne(id);
		if (!categoriaproductoDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the CategoriaProducto dosent exist");
		}
		return ResponseEntity.ok().body(categoriaproductoDTO2.get());
	}

	@GetMapping("/categoriaproductos")
	public ResponseEntity<List<CategoriaProductoDTO>> findAll(Pageable pageable) {
		Page<CategoriaProductoDTO> page = this.categoriaproductoService.findAll(pageable);
		HttpHeaders response = new HttpHeaders();
		response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
	}

	@DeleteMapping("/categoriaproductos/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.categoriaproductoService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

	@GetMapping("/categoriaproductos/search")
	public ResponseEntity<List<CategoriaProductoDTO>> searchColorsByKeyword(@RequestParam String keyword,
			Pageable pageable) {
		Page<CategoriaProductoDTO> page = this.categoriaproductoService.searchByKeyword(keyword, pageable);
		HttpHeaders response = new HttpHeaders();
		response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
	}
}
