package com.hulk.store.web.rest.vm;

import java.time.Instant;

import com.hulk.store.domain.CategoriaProducto;
import com.hulk.store.domain.Color;
import com.hulk.store.domain.EtiquetaProducto;
import com.hulk.store.domain.Talla;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ProductoVM {

    private Long id;
    private String nombre;
    private String descripcion;
    private String codigo;
    private Double costoUnitarioCompra;
    private Double precioUnitarioVenta;
    private Integer cantidad;
    private Boolean estado;
    private CategoriaProducto categoria;
    private EtiquetaProducto etiqueta;
    private Long categoriaId;
    private Long etiquetaId;
    private String imageUrl;
    private String createdBy;
    private Instant createdDate;
    private String lastModifiedBy;
    private Instant lastModifiedDate;
    // Comic
    private String titulo;
    private String genero;
    private Integer volumen;
    private Integer anioLanzamiento;
    private String idioma;
    // Camiseta
    private String sexo;
    private Color color;
    private Talla talla;
    private Long colorId;
    private Long tallaId;

}
