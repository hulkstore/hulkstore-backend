package com.hulk.store.web.rest.vm;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

import com.hulk.store.domain.CategoriaProducto;
import com.hulk.store.domain.EtiquetaProducto;
import com.hulk.store.domain.Imagen;
import com.hulk.store.domain.Color;
import com.hulk.store.domain.Talla;

import lombok.Getter;
import lombok.Setter;

import com.hulk.store.domain.Producto;

@Setter
@Getter
public class PreProductoVM implements Serializable {

  private Long id;
  private String nombre;
  private String descripcion;
  private String codigo;
  private Double costoUnitarioCompra;
  private Double precioUnitarioVenta;
  private Integer cantidad;
  private Boolean estado;
  private CategoriaProducto categoria;
  private EtiquetaProducto etiqueta;
  private Producto producto;
  private Long categoriaId;
  private Long etiquetaId;
  private String imageUrl;
  private String createdBy;
  private Instant createdDate;
  private String lastModifiedBy;
  private Instant lastModifiedDate;
  // Comic
  private String titulo;
  private String genero;
  private Integer volumen;
  private Integer anioLanzamiento;
  private String idioma;
  // Camiseta
  private String sexo;
  private Color color;
  private Talla talla;
  private Long colorId;
  private Long tallaId;

  public PreProductoVM() {
  }

}