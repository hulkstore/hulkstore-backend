package com.hulk.store.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.ProductoComicDTO;
import com.hulk.store.service.ProductoComicService;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class ProductoComicResource {

	private final ProductoComicService productocomicService;

	public ProductoComicResource(ProductoComicService productocomicService) {
		this.productocomicService = productocomicService;
	}

	@PostMapping("/productocomics")
	public ResponseEntity<ProductoComicDTO> create(@RequestBody ProductoComicDTO productocomicDTO) {
		if (productocomicDTO.getId() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID null");
		}
		ProductoComicDTO productocomicDTO2 = this.productocomicService.save(productocomicDTO);
		return ResponseEntity.ok().body(productocomicDTO2);
	}

	@PutMapping("/productocomics")
	public ResponseEntity<ProductoComicDTO> update(@RequestBody ProductoComicDTO productocomicDTO) {
		Optional<ProductoComicDTO> existingProductoComic = this.productocomicService.findOne(productocomicDTO.getId());
		if (existingProductoComic.isPresent()
				&& (!existingProductoComic.get().getId().equals(productocomicDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the ProductoComic already exists");
		}
		ProductoComicDTO productocomicDTO2 = this.productocomicService.save(productocomicDTO);
		return ResponseEntity.ok().body(productocomicDTO2);
	}

	@GetMapping("/productocomics/{id}")
	public ResponseEntity<ProductoComicDTO> findOne(@PathVariable Long id) {
		Optional<ProductoComicDTO> productocomicDTO2 = this.productocomicService.findOne(id);
		if (!productocomicDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the ProductoComic dosent exist");
		}
		return ResponseEntity.ok().body(productocomicDTO2.get());
	}

	@GetMapping("/productocomics")
	public ResponseEntity<List<ProductoComicDTO>> findAll(Pageable pageable) {
		Page<ProductoComicDTO> response = this.productocomicService.findAll(pageable);
		List<ProductoComicDTO> lista = response.getContent();
		return ResponseEntity.ok().body(lista);
	}

	@DeleteMapping("/productocomics/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.productocomicService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

}
