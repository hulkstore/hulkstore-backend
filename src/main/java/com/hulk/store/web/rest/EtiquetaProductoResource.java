package com.hulk.store.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.EtiquetaProductoDTO;
import com.hulk.store.config.Constants;
import com.hulk.store.domain.EtiquetaProducto;
import com.hulk.store.service.EtiquetaProductoService;
import com.hulk.store.repository.EtiquetaProductoRepository;
import com.hulk.store.config.Constants;
import org.springframework.http.HttpHeaders;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class EtiquetaProductoResource {

	private final EtiquetaProductoService etiquetaproductoService;

	private final EtiquetaProductoRepository etiquetaProductoRepository;

	public EtiquetaProductoResource(EtiquetaProductoService etiquetaproductoService,
			EtiquetaProductoRepository etiquetaProductoRepository) {
		this.etiquetaproductoService = etiquetaproductoService;
		this.etiquetaProductoRepository = etiquetaProductoRepository;
	}

	@PostMapping("/etiquetaproductos")
	public ResponseEntity<EtiquetaProductoDTO> create(@RequestBody EtiquetaProductoDTO etiquetaproductoDTO) {
		if (etiquetaproductoDTO.getId() != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID null");
		} else if (etiquetaProductoRepository.findOneByNombre(etiquetaproductoDTO.getNombre()).isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Etiqueta ya esta registrada");
		}
		EtiquetaProductoDTO etiquetaproductoDTO2 = this.etiquetaproductoService.save(etiquetaproductoDTO);
		return ResponseEntity.ok().body(etiquetaproductoDTO2);
	}

	@PutMapping("/etiquetaproductos")
	public ResponseEntity<EtiquetaProductoDTO> update(@RequestBody EtiquetaProductoDTO etiquetaproductoDTO) {
		Optional<EtiquetaProductoDTO> existingEtiquetaProducto = this.etiquetaproductoService
				.findOne(etiquetaproductoDTO.getId());
		if (existingEtiquetaProducto.isPresent()
				&& (!existingEtiquetaProducto.get().getId().equals(etiquetaproductoDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the EtiquetaProducto already exists");
		}
		Optional<EtiquetaProducto> existingEtiquetaProducto2 = etiquetaProductoRepository
				.findOneByNombre(etiquetaproductoDTO.getNombre());
		if (existingEtiquetaProducto2.isPresent()
				&& (!existingEtiquetaProducto2.get().getId().equals(etiquetaproductoDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "La Etiqueta ya existe");
		}
		EtiquetaProductoDTO etiquetaproductoDTO2 = this.etiquetaproductoService.save(etiquetaproductoDTO);
		return ResponseEntity.ok().body(etiquetaproductoDTO2);
	}

	@GetMapping("/etiquetaproductos/{id}")
	public ResponseEntity<EtiquetaProductoDTO> findOne(@PathVariable Long id) {
		Optional<EtiquetaProductoDTO> etiquetaproductoDTO2 = this.etiquetaproductoService.findOne(id);
		if (!etiquetaproductoDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the EtiquetaProducto dosent exist");
		}
		return ResponseEntity.ok().body(etiquetaproductoDTO2.get());
	}

	@GetMapping("/etiquetaproductos")
	public ResponseEntity<List<EtiquetaProductoDTO>> findAll(Pageable pageable) {
		Page<EtiquetaProductoDTO> page = this.etiquetaproductoService.findAll(pageable);
		HttpHeaders response = new HttpHeaders();
		response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
	}

	@DeleteMapping("/etiquetaproductos/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.etiquetaproductoService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

	@GetMapping("/etiquetaproductos/search")
	public ResponseEntity<List<EtiquetaProductoDTO>> searchColorsByKeyword(@RequestParam String keyword,
			Pageable pageable) {
		Page<EtiquetaProductoDTO> page = this.etiquetaproductoService.searchByKeyword(keyword, pageable);
		HttpHeaders response = new HttpHeaders();
		response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
	}
}
