package com.hulk.store.web.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import com.hulk.store.web.rest.vm.PreProductoVM;
import com.hulk.store.web.rest.vm.ProductoVM;
import com.hulk.store.dto.ProductoDTO;
import com.hulk.store.repository.ProductoRepository;
import com.hulk.store.domain.ProductoCamiseta;
import com.hulk.store.dto.CategoriaProductoDTO;
import com.hulk.store.dto.ImagenDTO;
import com.hulk.store.dto.ProductoCamisetaDTO;
import com.hulk.store.dto.ProductoComicDTO;
import com.hulk.store.service.ProductoService;
import com.hulk.store.service.ProductoComicService;
import com.hulk.store.service.CategoriaProductoService;
import com.hulk.store.service.ProductoCamisetaService;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class ProductoController {
  public static final String XTOTALCOUNT = "X-total-count";

  private final ProductoService productoService;
  private final ProductoComicService productoComicService;
  private final ProductoCamisetaService productoCamisetaService;
  private final ProductoRepository productoRepository;
  private final CategoriaProductoService categoriaProductoService;

  public ProductoController(ProductoService productoService, ProductoComicService productoComicService,
      ProductoCamisetaService productoCamisetaService, ProductoRepository productoRepository,
      CategoriaProductoService categoriaProductoService) {
    this.productoService = productoService;
    this.productoComicService = productoComicService;
    this.productoCamisetaService = productoCamisetaService;
    this.productoRepository = productoRepository;
    this.categoriaProductoService = categoriaProductoService;
  }

  @PostMapping("/productos/save-producto")
  public ResponseEntity<ProductoVM> saveProducto(@RequestBody ProductoVM preProductoVM) {
    ProductoDTO productoDTO = new ProductoDTO();

    String code = this.productoService.generateCodeByProduct(preProductoVM.getNombre());
    productoDTO.setCodigo(code);
    if (this.productoRepository.findOneByCodigo(productoDTO.getCodigo()).isPresent()) {
      code = this.productoService.generateCodeByProduct(productoDTO.getNombre());
      productoDTO.setCodigo(code);
    }

    productoDTO.setId(null);
    productoDTO.setNombre(preProductoVM.getNombre());
    productoDTO.setDescripcion(preProductoVM.getDescripcion());
    productoDTO.setCostoUnitarioCompra(preProductoVM.getCostoUnitarioCompra());
    productoDTO.setPrecioUnitarioVenta(preProductoVM.getPrecioUnitarioVenta());
    productoDTO.setCantidad(0);
    productoDTO.setEstado(false);
    productoDTO.setCategoriaId(preProductoVM.getCategoriaId());
    productoDTO.setEtiquetaId(preProductoVM.getEtiquetaId());
    ProductoDTO productoDTO2 = productoService.save(productoDTO);

    Optional<CategoriaProductoDTO> categoriaProductoDTO = this.categoriaProductoService
        .findOne(preProductoVM.getCategoriaId());

    if (categoriaProductoDTO.isPresent()) {
      if (categoriaProductoDTO.get().getNombre().equals("COMICS")) {
        ProductoComicDTO productoComicDTO = new ProductoComicDTO();
        productoComicDTO.setId(null);
        productoComicDTO.setTitulo(preProductoVM.getTitulo());
        productoComicDTO.setGenero(preProductoVM.getGenero());
        productoComicDTO.setVolumen(preProductoVM.getVolumen());
        productoComicDTO.setAnioLanzamiento(preProductoVM.getAnioLanzamiento());
        productoComicDTO.setIdioma(preProductoVM.getIdioma());
        productoComicDTO.setProductoId(productoDTO2.getId());
        this.productoComicService.save(productoComicDTO);
      } else if (categoriaProductoDTO.get().getNombre().equals("CAMISETA")) {
        ProductoCamisetaDTO productoCamisetaDTO = new ProductoCamisetaDTO();
        productoCamisetaDTO.setId(null);
        productoCamisetaDTO.setSexo(preProductoVM.getSexo());
        productoCamisetaDTO.setColorId(preProductoVM.getColorId());
        productoCamisetaDTO.setTallaId(preProductoVM.getTallaId());
        productoCamisetaDTO.setProductoId(productoDTO2.getId());
        this.productoCamisetaService.save(productoCamisetaDTO);
      }
    }

    preProductoVM.setCodigo(productoDTO2.getCodigo());
    preProductoVM.setId(productoDTO2.getId());

    return ResponseEntity.ok().body(preProductoVM);
  }

  @PutMapping("/productos/update-producto")
  public ResponseEntity<ProductoVM> updateProducto(@RequestBody ProductoVM preProductoVM) {
    ProductoDTO productoDTO = new ProductoDTO();

    productoDTO.setId(preProductoVM.getId());
    productoDTO.setNombre(preProductoVM.getNombre());
    productoDTO.setDescripcion(preProductoVM.getDescripcion());
    productoDTO.setCostoUnitarioCompra(preProductoVM.getCostoUnitarioCompra());
    productoDTO.setPrecioUnitarioVenta(preProductoVM.getPrecioUnitarioVenta());
    productoDTO.setCantidad(0);
    productoDTO.setCodigo(preProductoVM.getCodigo());
    productoDTO.setEstado(false);
    productoDTO.setCategoriaId(preProductoVM.getCategoriaId());
    productoDTO.setEtiquetaId(preProductoVM.getEtiquetaId());
    List<ImagenDTO> imagenDTOs = new ArrayList<>();
    productoDTO.setImagenDTOs(imagenDTOs);

    ProductoDTO productoDTO2 = productoService.save(productoDTO);

    Optional<CategoriaProductoDTO> categoriaProductoDTO = this.categoriaProductoService
        .findOne(preProductoVM.getCategoriaId());

    if (categoriaProductoDTO.isPresent()) {
      if (categoriaProductoDTO.get().getNombre().equals("COMICS")) {
        ProductoComicDTO productoComicDTO = new ProductoComicDTO();
        productoComicDTO.setId(null);
        productoComicDTO.setTitulo(preProductoVM.getTitulo());
        productoComicDTO.setGenero(preProductoVM.getGenero());
        productoComicDTO.setVolumen(preProductoVM.getVolumen());
        productoComicDTO.setAnioLanzamiento(preProductoVM.getAnioLanzamiento());
        productoComicDTO.setIdioma(preProductoVM.getIdioma());
        productoComicDTO.setProductoId(productoDTO2.getId());
        this.productoComicService.save(productoComicDTO);
      } else if (categoriaProductoDTO.get().getNombre().equals("CAMISETA")) {
        ProductoCamisetaDTO productoCamisetaDTO = new ProductoCamisetaDTO();
        productoCamisetaDTO.setId(null);
        productoCamisetaDTO.setSexo(preProductoVM.getSexo());
        productoCamisetaDTO.setColorId(preProductoVM.getColorId());
        productoCamisetaDTO.setTallaId(preProductoVM.getTallaId());
        productoCamisetaDTO.setProductoId(productoDTO2.getId());
        this.productoCamisetaService.save(productoCamisetaDTO);
      }
    }

    preProductoVM.setCodigo(productoDTO2.getCodigo());
    preProductoVM.setId(productoDTO2.getId());

    return ResponseEntity.ok().body(preProductoVM);
  }

  @GetMapping("/productos/get-one/{id}")
  public ResponseEntity<PreProductoVM> findOne(@PathVariable Long id) {
    Optional<PreProductoVM> preProductoVM = this.productoService.findOneProduct(id);
    if (!preProductoVM.isPresent()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the Producto dosent exist");
    }
    return ResponseEntity.ok().body(preProductoVM.get());
  }

  @GetMapping("/productos/get-all")
  public ResponseEntity<List<PreProductoVM>> findAll(Pageable pageable) {
    Page<PreProductoVM> page = this.productoService.findAllProducts(pageable);
    HttpHeaders response = new HttpHeaders();
    response.add(XTOTALCOUNT, String.valueOf(page.getTotalElements()));
    return ResponseEntity.ok().headers(response).body(page.getContent());
  }

}