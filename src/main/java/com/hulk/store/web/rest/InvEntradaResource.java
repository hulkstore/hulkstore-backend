package com.hulk.store.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.InvEntradaDTO;
import com.hulk.store.service.InvEntradaService;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class InvEntradaResource {

	private final InvEntradaService inventradaService;

	public InvEntradaResource(InvEntradaService inventradaService) {
		this.inventradaService = inventradaService;
	}

	@PostMapping("/inventradas")
	public ResponseEntity<InvEntradaDTO> create(@RequestBody InvEntradaDTO inventradaDTO) {
		if (inventradaDTO.getId() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID null");
		}
		InvEntradaDTO inventradaDTO2 = this.inventradaService.save(inventradaDTO);
		return ResponseEntity.ok().body(inventradaDTO2);
	}

	@PutMapping("/inventradas")
	public ResponseEntity<InvEntradaDTO> update(@RequestBody InvEntradaDTO inventradaDTO) {
		Optional<InvEntradaDTO> existingInvEntrada = this.inventradaService.findOne(inventradaDTO.getId());
		if (existingInvEntrada.isPresent() && (!existingInvEntrada.get().getId().equals(inventradaDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the InvEntrada already exists");
		}
		InvEntradaDTO inventradaDTO2 = this.inventradaService.save(inventradaDTO);
		return ResponseEntity.ok().body(inventradaDTO2);
	}

	@GetMapping("/inventradas/{id}")
	public ResponseEntity<InvEntradaDTO> findOne(@PathVariable Long id) {
		Optional<InvEntradaDTO> inventradaDTO2 = this.inventradaService.findOne(id);
		if (!inventradaDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the InvEntrada dosent exist");
		}
		return ResponseEntity.ok().body(inventradaDTO2.get());
	}

	@GetMapping("/inventradas")
	public ResponseEntity<List<InvEntradaDTO>> findAll(Pageable pageable) {
		Page<InvEntradaDTO> response = this.inventradaService.findAll(pageable);
		List<InvEntradaDTO> lista = response.getContent();
		return ResponseEntity.ok().body(lista);
	}

	@DeleteMapping("/inventradas/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.inventradaService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

}
