package com.hulk.store.web.rest;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.UsuarioDTO;
import com.hulk.store.service.UsuarioService;
import com.hulk.store.repository.UsuarioRepository;
import com.hulk.store.config.Constants;
import com.hulk.store.domain.Usuario;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class UsuarioResource {

	private final UsuarioService usuarioService;
	private final UsuarioRepository usuarioRepository;

	public UsuarioResource(UsuarioService usuarioService, UsuarioRepository usuarioRepository) {
		this.usuarioService = usuarioService;
		this.usuarioRepository = usuarioRepository;
	}

	@PostMapping("/usuarios")
	public ResponseEntity<UsuarioDTO> create(@RequestBody UsuarioDTO usuarioDTO) {
		if (usuarioDTO.getId() != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID no es null");
		} else if (usuarioRepository.findOneByEmail(usuarioDTO.getEmail()).isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email ya esta registrado");
		} else if (usuarioRepository.findOneByUsername(usuarioDTO.getUsername()).isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Username ya esta registrado");
		}
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(); // Bcrypt
		String password = passwordEncoder.encode(usuarioDTO.getPassword()); // encode password
		usuarioDTO.setPassword(password);
		UsuarioDTO usuarioDTO2 = this.usuarioService.save(usuarioDTO);
		return ResponseEntity.ok().body(usuarioDTO2);
	}

	@PutMapping("/usuarios")
	public ResponseEntity<UsuarioDTO> update(@RequestBody UsuarioDTO usuarioDTO) {
		Optional<UsuarioDTO> existingUsuario = this.usuarioService.findOne(usuarioDTO.getId());
		if (existingUsuario.isPresent() && (!existingUsuario.get().getId().equals(usuarioDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the Usuario already exists");
		}
		Optional<Usuario> existingUsuario2 = usuarioRepository.findOneByEmail(usuarioDTO.getEmail());
		if (existingUsuario2.isPresent() && (!existingUsuario2.get().getId().equals(usuarioDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email registrado exists");
		}
		existingUsuario2 = usuarioRepository.findOneByUsername(usuarioDTO.getUsername());
		if (existingUsuario2.isPresent() && (!existingUsuario2.get().getId().equals(usuarioDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Username registrado exists");
		}
		if(usuarioDTO.getPassword() == null || usuarioDTO.getPassword().isBlank()) {
            usuarioDTO.setPassword(existingUsuario.get().getPassword());
        }  else {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(); // Bcrypt
            String encryptedPassword = passwordEncoder.encode(usuarioDTO.getPassword()); // encode password
            usuarioDTO.setPassword(encryptedPassword);
        }
		UsuarioDTO usuarioDTO2 = this.usuarioService.save(usuarioDTO);
		return ResponseEntity.ok().body(usuarioDTO2);
	}

	@GetMapping("/usuarios/{id}")
	public ResponseEntity<UsuarioDTO> findOne(@PathVariable Long id) {
		Optional<UsuarioDTO> usuarioDTO2 = this.usuarioService.findOne(id);
		if (!usuarioDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the Usuario dosent exist");
		}
		return ResponseEntity.ok().body(usuarioDTO2.get());
	}

	@GetMapping("/usuarios")
	public ResponseEntity<List<UsuarioDTO>> findAll(Pageable pageable) {
		Page<UsuarioDTO> page = this.usuarioService.findAll(pageable);
        HttpHeaders response = new HttpHeaders();
    	response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
	}

	@DeleteMapping("/usuarios/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.usuarioService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

}
