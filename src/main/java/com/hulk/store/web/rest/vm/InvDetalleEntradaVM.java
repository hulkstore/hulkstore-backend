package com.hulk.store.web.rest.vm;

import java.time.Instant;

import com.hulk.store.domain.InvEntrada;
import com.hulk.store.domain.Producto;

import lombok.*;

@Getter
@Setter
public class InvDetalleEntradaVM {
  private Long id;
  private Double costoUnitario;
  private Integer cantidad;
  private Long productoId;
  private Producto producto;
  private Long entradaId;
  // private InvEntrada invEntrada;
  private Double subTotal;
  private String createdBy;
  private Instant createdDate;
  private String lastModifiedBy;
  private Instant lastModifiedDate;
}
