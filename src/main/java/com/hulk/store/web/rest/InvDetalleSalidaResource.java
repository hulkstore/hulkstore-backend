package com.hulk.store.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.InvDetalleSalidaDTO;
import com.hulk.store.service.InvDetalleSalidaService;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class InvDetalleSalidaResource {

	private final InvDetalleSalidaService invdetallesalidaService;

	public InvDetalleSalidaResource(InvDetalleSalidaService invdetallesalidaService) {
		this.invdetallesalidaService = invdetallesalidaService;
	}

	@PostMapping("/invdetallesalidas")
	public ResponseEntity<InvDetalleSalidaDTO> create(@RequestBody InvDetalleSalidaDTO invdetallesalidaDTO) {
		if (invdetallesalidaDTO.getId() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID null");
		}
		InvDetalleSalidaDTO invdetallesalidaDTO2 = this.invdetallesalidaService.save(invdetallesalidaDTO);
		return ResponseEntity.ok().body(invdetallesalidaDTO2);
	}

	@PutMapping("/invdetallesalidas")
	public ResponseEntity<InvDetalleSalidaDTO> update(@RequestBody InvDetalleSalidaDTO invdetallesalidaDTO) {
		Optional<InvDetalleSalidaDTO> existingInvDetalleSalida = this.invdetallesalidaService
				.findOne(invdetallesalidaDTO.getId());
		if (existingInvDetalleSalida.isPresent()
				&& (!existingInvDetalleSalida.get().getId().equals(invdetallesalidaDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the InvDetalleSalida already exists");
		}
		InvDetalleSalidaDTO invdetallesalidaDTO2 = this.invdetallesalidaService.save(invdetallesalidaDTO);
		return ResponseEntity.ok().body(invdetallesalidaDTO2);
	}

	@GetMapping("/invdetallesalidas/{id}")
	public ResponseEntity<InvDetalleSalidaDTO> findOne(@PathVariable Long id) {
		Optional<InvDetalleSalidaDTO> invdetallesalidaDTO2 = this.invdetallesalidaService.findOne(id);
		if (!invdetallesalidaDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the InvDetalleSalida dosent exist");
		}
		return ResponseEntity.ok().body(invdetallesalidaDTO2.get());
	}

	@GetMapping("/invdetallesalidas")
	public ResponseEntity<List<InvDetalleSalidaDTO>> findAll(Pageable pageable) {
		Page<InvDetalleSalidaDTO> response = this.invdetallesalidaService.findAll(pageable);
		List<InvDetalleSalidaDTO> lista = response.getContent();
		return ResponseEntity.ok().body(lista);
	}

	@DeleteMapping("/invdetallesalidas/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.invdetallesalidaService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

}
