package com.hulk.store.web.rest;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.hulk.store.config.Constants;
import com.hulk.store.dto.ImagenDTO;
import com.hulk.store.service.ImagenService;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class ImagenResource {

	private final ImagenService imagenService;

	public ImagenResource(ImagenService imagenService) {
		this.imagenService = imagenService;
	}

	@PostMapping("/imagens")
	public ResponseEntity<ImagenDTO> create(@RequestBody ImagenDTO imagenDTO) {
		if (imagenDTO.getId() != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID null");
		}
		ImagenDTO imagenDTO2 = this.imagenService.save(imagenDTO);
		return ResponseEntity.ok().body(imagenDTO2);
	}

	@PutMapping("/imagens")
	public ResponseEntity<ImagenDTO> update(@RequestBody ImagenDTO imagenDTO) {
		Optional<ImagenDTO> existingImagen = this.imagenService.findOne(imagenDTO.getId());
		if (existingImagen.isPresent() && (!existingImagen.get().getId().equals(imagenDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the Imagen already exists");
		}
		ImagenDTO imagenDTO2 = this.imagenService.save(imagenDTO);
		return ResponseEntity.ok().body(imagenDTO2);
	}

	@GetMapping("/imagens/{id}")
	public ResponseEntity<ImagenDTO> findOne(@PathVariable Long id) {
		Optional<ImagenDTO> imagenDTO2 = this.imagenService.findOne(id);
		if (!imagenDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the Imagen dosent exist");
		}
		return ResponseEntity.ok().body(imagenDTO2.get());
	}

	@GetMapping("/imagens")
	public ResponseEntity<List<ImagenDTO>> findAll() {
		List<ImagenDTO> response = this.imagenService.findAll();
		return ResponseEntity.ok().body(response);
	}

	@DeleteMapping("/imagens/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.imagenService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

	@GetMapping("/imagens/product-id/{id}")
    public ResponseEntity<List<ImagenDTO>> getAllImagensByProductId(@PathVariable Long id, Pageable pageable) {
        Page<ImagenDTO> page = imagenService.findAllImagesByProductId(id, pageable);
		HttpHeaders response = new HttpHeaders();
		response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
    }

	@PostMapping("/imagens/post/upload-product-images")
    public ResponseEntity<ImagenDTO> uploadPostProductImage(
            @RequestParam("productId") Long id,
            @RequestParam("file") MultipartFile file) throws IllegalStateException, IOException {
        String home = System.getProperty("user.home");
        String dirLocation = home + "/.images/product/images/";
        String dir = dirLocation;
        String typeImage = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1, file.getOriginalFilename().length());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String fileName = "";
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String tipoImagen = "";
        if (typeImage.equals("jpg")) {
            tipoImagen = "jpg";
            fileName = "id_" + id.toString() + "_" + sdf.format(new Date()) + timestamp.getTime() + ".jpg";
        } else if (typeImage.equals("png")) {
            tipoImagen = "png";
            fileName = "id_" + id.toString() + "_" + sdf.format(new Date()) + timestamp.getTime() + ".png";
        } else if (typeImage.equals("jpeg")) {
            tipoImagen = "jpeg";
            fileName = "id_" + id.toString() + "_" + sdf.format(new Date()) + timestamp.getTime() + ".jpeg";
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "formato de imagen invalido");
        }
        File dirLoc = new File(dir);
        if (!dirLoc.exists()) {
            dirLoc.mkdirs();
        }
        File secondFile = new File(dirLoc.getAbsolutePath() + "/" + fileName);
        file.transferTo(secondFile);
        String imageUrl = "/product" + "/get-image/" + fileName + "?url=" + "/product/images/";
        ImagenDTO imagenDTO = new ImagenDTO();
        imagenDTO.setUrl(imageUrl);
        imagenDTO.setNombre(fileName);
        imagenDTO.setTipo(tipoImagen);
        imagenDTO.setProductoId(id);
        this.imagenService.save(imagenDTO);
        return ResponseEntity.ok().body(imagenDTO);
    }

}
