package com.hulk.store.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.UsuarioDTO;
import com.hulk.store.repository.UsuarioRepository;
import com.hulk.store.service.UsuarioService;
import com.hulk.store.service.mapper.UsuarioMapper;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class AccountResource {
    
    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UsuarioRepository usuarioRepository;

    private final UsuarioService usuarioService;

    private final UsuarioMapper usuarioMapper;

    public AccountResource(UsuarioRepository usuarioRepository, UsuarioService usuarioService, UsuarioMapper usuarioMapper) {
        this.usuarioRepository = usuarioRepository;
        this.usuarioService = usuarioService;
        this.usuarioMapper = usuarioMapper;
    }

    @GetMapping("/account")
    public UsuarioDTO getAccount() {
        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        UsuarioDTO userDTO = this.usuarioRepository.findOneByUsername(currentUser)
            .map(t -> this.usuarioMapper.toDTO(t))
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "El usuario no pudo ser encontrado!"));
        return userDTO;
    }

}
