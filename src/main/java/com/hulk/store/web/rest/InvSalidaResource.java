package com.hulk.store.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.InvSalidaDTO;
import com.hulk.store.service.InvSalidaService;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class InvSalidaResource {

	private final InvSalidaService invsalidaService;

	public InvSalidaResource(InvSalidaService invsalidaService) {
		this.invsalidaService = invsalidaService;
	}

	@PostMapping("/invsalidas")
	public ResponseEntity<InvSalidaDTO> create(@RequestBody InvSalidaDTO invsalidaDTO) {
		if (invsalidaDTO.getId() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID null");
		}
		InvSalidaDTO invsalidaDTO2 = this.invsalidaService.save(invsalidaDTO);
		return ResponseEntity.ok().body(invsalidaDTO2);
	}

	@PutMapping("/invsalidas")
	public ResponseEntity<InvSalidaDTO> update(@RequestBody InvSalidaDTO invsalidaDTO) {
		Optional<InvSalidaDTO> existingInvSalida = this.invsalidaService.findOne(invsalidaDTO.getId());
		if (existingInvSalida.isPresent() && (!existingInvSalida.get().getId().equals(invsalidaDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the InvSalida already exists");
		}
		InvSalidaDTO invsalidaDTO2 = this.invsalidaService.save(invsalidaDTO);
		return ResponseEntity.ok().body(invsalidaDTO2);
	}

	@GetMapping("/invsalidas/{id}")
	public ResponseEntity<InvSalidaDTO> findOne(@PathVariable Long id) {
		Optional<InvSalidaDTO> invsalidaDTO2 = this.invsalidaService.findOne(id);
		if (!invsalidaDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the InvSalida dosent exist");
		}
		return ResponseEntity.ok().body(invsalidaDTO2.get());
	}

	@GetMapping("/invsalidas")
	public ResponseEntity<List<InvSalidaDTO>> findAll(Pageable pageable) {
		Page<InvSalidaDTO> response = this.invsalidaService.findAll(pageable);
		List<InvSalidaDTO> lista = response.getContent();
		return ResponseEntity.ok().body(lista);
	}

	@DeleteMapping("/invsalidas/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.invsalidaService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

}
