package com.hulk.store.web.rest;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hulk.store.service.RolService;
import com.hulk.store.dto.RolDTO;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class RolResource {

  private final RolService rolService;

  public RolResource(RolService rolService) {
    this.rolService = rolService;
  }

  @GetMapping("/roles")
  public ResponseEntity<List<RolDTO>> findAll() {
    List<RolDTO> response = this.rolService.findAll();
    return ResponseEntity.ok().body(response);
  }

}
