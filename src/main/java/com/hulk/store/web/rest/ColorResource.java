package com.hulk.store.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.ColorDTO;
import com.hulk.store.config.Constants;
import com.hulk.store.domain.Color;
import com.hulk.store.service.ColorService;
import com.hulk.store.repository.ColorRepository;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class ColorResource {

	private final ColorService colorService;
	private final ColorRepository colorRepository;

	public ColorResource(ColorService colorService, ColorRepository colorRepository) {
		this.colorService = colorService;
		this.colorRepository = colorRepository;
	}

	@PostMapping("/colors")
	public ResponseEntity<ColorDTO> create(@RequestBody ColorDTO colorDTO) {
		if (colorDTO.getId() != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El Id es null");
		} else if (colorRepository.findOneByCodigo(colorDTO.getCodigo()).isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El codigo ya esta registrado");
		}
		ColorDTO colorDTO2 = this.colorService.save(colorDTO);
		return ResponseEntity.ok().body(colorDTO2);
	}

	@PutMapping("/colors")
	public ResponseEntity<ColorDTO> update(@RequestBody ColorDTO colorDTO) {
		Optional<ColorDTO> existingColor = this.colorService.findOne(colorDTO.getId());
		if (existingColor.isPresent() && (!existingColor.get().getId().equals(colorDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the Color already exists");
		}
		Optional<Color> existingColor2 = colorRepository.findOneByCodigo(colorDTO.getCodigo());
		if (existingColor2.isPresent() && (!existingColor2.get().getId().equals(colorDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El codigo ya esta registrado");
		}
		ColorDTO colorDTO2 = this.colorService.save(colorDTO);
		return ResponseEntity.ok().body(colorDTO2);
	}

	@GetMapping("/colors/{id}")
	public ResponseEntity<ColorDTO> findOne(@PathVariable Long id) {
		Optional<ColorDTO> colorDTO2 = this.colorService.findOne(id);
		if (!colorDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the Color dosent exist");
		}
		return ResponseEntity.ok().body(colorDTO2.get());
	}

	@GetMapping("/colors")
	public ResponseEntity<List<ColorDTO>> findAll(Pageable pageable) {
		Page<ColorDTO> page = this.colorService.findAll(pageable);
        HttpHeaders response = new HttpHeaders();
    	response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
        return ResponseEntity.ok().headers(response).body(page.getContent());
	}

	@DeleteMapping("/colors/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.colorService.deleteOne(id);
		return ResponseEntity.ok().body("Eliminado");
	}

    @GetMapping("/colors/search")
    public ResponseEntity<List<ColorDTO>> searchColorsByKeyword(@RequestParam String keyword, Pageable pageable) {
        Page<ColorDTO> page = this.colorService.searchByKeyword(keyword, pageable);
        HttpHeaders response = new HttpHeaders();
    	response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
        return ResponseEntity.ok().headers(response).body(page.getContent());
    }
	
}
