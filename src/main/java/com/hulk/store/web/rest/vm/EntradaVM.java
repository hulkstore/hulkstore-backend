package com.hulk.store.web.rest.vm;

import com.hulk.store.domain.InvEntrada;
import com.hulk.store.domain.InvDetalleEntrada;
import java.time.Instant;
import java.util.List;
import lombok.*;

@Getter
@Setter
public class EntradaVM {
  private Long id;
  private Boolean estado;
  private Double total;
  private List<InvDetalleEntrada> invDetalleEntradas;
  private List<InvDetalleEntradaVM> invDetalleEntradaVMs;
  private String createdBy;
  private Instant createdDate;
  private String lastModifiedBy;
  private Instant lastModifiedDate;

  public EntradaVM() {
  }

  public static EntradaVM parse(InvEntrada invEntrada, List<InvDetalleEntrada> invDetalleEntrada) {
    EntradaVM entradaVM = new EntradaVM();
    entradaVM.setId(invEntrada.getId());
    entradaVM.setEstado(invEntrada.getEstado());
    entradaVM.setInvDetalleEntradas(invDetalleEntrada);
    entradaVM.setCreatedBy(invEntrada.getCreatedBy());
    entradaVM.setCreatedDate(invEntrada.getCreatedDate());
    entradaVM.setLastModifiedBy(invEntrada.getLastModifiedBy());
    entradaVM.setLastModifiedDate(invEntrada.getLastModifiedDate());
    return entradaVM;
  }
}