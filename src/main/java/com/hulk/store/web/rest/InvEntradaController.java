package com.hulk.store.web.rest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.hulk.store.dto.InvDetalleEntradaDTO;
import com.hulk.store.dto.InvEntradaDTO;
import com.hulk.store.web.rest.vm.EntradaVM;
import com.hulk.store.web.rest.vm.InvDetalleEntradaVM;
import com.hulk.store.service.InvEntradaService;
import com.hulk.store.repository.ProductoRepository;
import com.hulk.store.domain.Producto;
import com.hulk.store.service.InvDetalleEntradaService;
import com.hulk.store.config.Constants;
import org.springframework.http.HttpHeaders;
import org.springframework.data.domain.Pageable;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class InvEntradaController {

  private final InvEntradaService invEntradaService;
  private final ProductoRepository productoRepository;
  private final InvDetalleEntradaService invDetalleEntradaService;

  public InvEntradaController(InvEntradaService invEntradaService, ProductoRepository productoRepository,
      InvDetalleEntradaService invDetalleEntradaService) {
    this.invEntradaService = invEntradaService;
    this.productoRepository = productoRepository;
    this.invDetalleEntradaService = invDetalleEntradaService;
  }

  @PostMapping(value = "/inventradas/save-entrada")
  public ResponseEntity<String> create(@RequestPart("entradaVM") EntradaVM entradaVM) {

    if (entradaVM.getId() != null) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID exist");
    }
    InvEntradaDTO inventradaDTO = new InvEntradaDTO();
    inventradaDTO.setId(null);
    inventradaDTO.setEstado(true);
    InvEntradaDTO invEntradaDTO2 = invEntradaService.save(inventradaDTO);
    Double total = 0.0;
    Double subtotal = 0.0;
    for (InvDetalleEntradaVM invDetalleEntradaVM : entradaVM.getInvDetalleEntradaVMs()) {
      InvDetalleEntradaDTO invDetalleEntradaDTO = new InvDetalleEntradaDTO();
      invDetalleEntradaDTO.setId(null);
      invDetalleEntradaDTO.setCostoUnitario(invDetalleEntradaVM.getCostoUnitario());
      invDetalleEntradaDTO.setCantidad(invDetalleEntradaVM.getCantidad());
      // subtotal = invDetalleEntradaVM.getCostoUnitario() *
      // invDetalleEntradaVM.getCantidad();
      // invDetalleEntradaDTO.setSubTotal(invDetalleEntradaVM.getSubTotal());
      invDetalleEntradaDTO.setProductoId(invDetalleEntradaVM.getProductoId());
      invDetalleEntradaDTO.setInvEntradaId(invEntradaDTO2.getId());
      Optional<Producto> producto = productoRepository.findById(invDetalleEntradaVM.getProductoId());
      if (producto.isPresent()) {
        Integer cantidad = producto.get().getCantidad();
        cantidad += invDetalleEntradaVM.getCantidad();
        producto.get().setCantidad(cantidad);
        producto.get().setEstado(true);
      }
      total += subtotal;
      invDetalleEntradaService.save(invDetalleEntradaDTO);
    }
    invEntradaDTO2.setTotal(total);
    invEntradaService.save(invEntradaDTO2);
    return ResponseEntity.ok().body("exito");
  }

  @GetMapping("/inventradas/get-one/{id}")
  public ResponseEntity<EntradaVM> findOne(@PathVariable Long id) {
    Optional<InvEntradaDTO> inventradaDTO2 = this.invEntradaService.findOne(id);
    if (!inventradaDTO2.isPresent()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the InvEntrada dosent exist");
    }
    Optional<EntradaVM> entradaVM = this.invEntradaService.findOneInvEntrada(id);
    return ResponseEntity.ok().body(entradaVM.get());
  }

  @GetMapping("/inventradas/get-all")
  public ResponseEntity<List<EntradaVM>> findAll(Pageable pageable) {
    Page<EntradaVM> page = this.invEntradaService.findAllInvEntrada(pageable);
    HttpHeaders response = new HttpHeaders();
    response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
    return ResponseEntity.ok().headers(response).body(page.getContent());
  }
}
