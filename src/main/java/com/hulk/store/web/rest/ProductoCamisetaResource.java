package com.hulk.store.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.hulk.store.dto.ProductoCamisetaDTO;
import com.hulk.store.service.ProductoCamisetaService;

@CrossOrigin(origins = "*", maxAge = 3000)
@RestController
@RequestMapping("/api")
public class ProductoCamisetaResource {

	private final ProductoCamisetaService productocamisetaService;

	public ProductoCamisetaResource(ProductoCamisetaService productocamisetaService) {
		this.productocamisetaService = productocamisetaService;
	}

	@PostMapping("/productocamisetas")
	public ResponseEntity<ProductoCamisetaDTO> create(@RequestBody ProductoCamisetaDTO productocamisetaDTO) {
		if (productocamisetaDTO.getId() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID null");
		}
		ProductoCamisetaDTO productocamisetaDTO2 = this.productocamisetaService.save(productocamisetaDTO);
		return ResponseEntity.ok().body(productocamisetaDTO2);
	}

	@PutMapping("/productocamisetas")
	public ResponseEntity<ProductoCamisetaDTO> update(@RequestBody ProductoCamisetaDTO productocamisetaDTO) {
		Optional<ProductoCamisetaDTO> existingProductoCamiseta = this.productocamisetaService
				.findOne(productocamisetaDTO.getId());
		if (existingProductoCamiseta.isPresent()
				&& (!existingProductoCamiseta.get().getId().equals(productocamisetaDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the ProductoCamiseta already exists");
		}
		ProductoCamisetaDTO productocamisetaDTO2 = this.productocamisetaService.save(productocamisetaDTO);
		return ResponseEntity.ok().body(productocamisetaDTO2);
	}

	@GetMapping("/productocamisetas/{id}")
	public ResponseEntity<ProductoCamisetaDTO> findOne(@PathVariable Long id) {
		Optional<ProductoCamisetaDTO> productocamisetaDTO2 = this.productocamisetaService.findOne(id);
		if (!productocamisetaDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the ProductoCamiseta dosent exist");
		}
		return ResponseEntity.ok().body(productocamisetaDTO2.get());
	}

	@GetMapping("/productocamisetas")
	public ResponseEntity<List<ProductoCamisetaDTO>> findAll(Pageable pageable) {
		Page<ProductoCamisetaDTO> response = this.productocamisetaService.findAll(pageable);
		List<ProductoCamisetaDTO> lista = response.getContent();
		return ResponseEntity.ok().body(lista);
	}

	@DeleteMapping("/productocamisetas/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.productocamisetaService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

}
