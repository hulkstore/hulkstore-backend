package com.hulk.store.web.rest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.hulk.store.config.Constants;
import com.hulk.store.dto.ProductoDTO;
import com.hulk.store.service.ProductoService;

@RestController
@RequestMapping("/api")
public class ProductoResource {

	private final ProductoService productoService;

	public ProductoResource(ProductoService productoService) {
		this.productoService = productoService;

	}

	@PostMapping("/productos")
	public ResponseEntity<ProductoDTO> create(@RequestBody ProductoDTO productoDTO) {
		if (productoDTO.getId() != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "EL Id no es null");
		}
		ProductoDTO productoDTO2 = this.productoService.save(productoDTO);
		return ResponseEntity.ok().body(productoDTO2);
	}

	@PutMapping("/productos")
	public ResponseEntity<ProductoDTO> update(@RequestBody ProductoDTO productoDTO) {
		Optional<ProductoDTO> existingProducto = this.productoService.findOne(productoDTO.getId());
		if (existingProducto.isPresent() && (!existingProducto.get().getId().equals(productoDTO.getId()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El producto ya existe");
		}
		ProductoDTO productoDTO2 = this.productoService.save(productoDTO);
		return ResponseEntity.ok().body(productoDTO2);
	}

	@GetMapping("/productos/{id}")
	public ResponseEntity<ProductoDTO> findOne(@PathVariable Long id) {
		Optional<ProductoDTO> productoDTO2 = this.productoService.findOne(id);
		if (!productoDTO2.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El producto no existe");
		}
		return ResponseEntity.ok().body(productoDTO2.get());
	}

	@GetMapping("/productos")
	public ResponseEntity<List<ProductoDTO>> findAll(Pageable pageable) {
		Page<ProductoDTO> page = this.productoService.findAll(pageable);
		HttpHeaders response = new HttpHeaders();
		response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
	}

	@DeleteMapping("/productos/{id}")
	public ResponseEntity<String> deleteOne(@PathVariable Long id) {
		this.productoService.deleteOne(id);
		return ResponseEntity.ok().body("Destroyed");
	}

	@GetMapping("/productos/search")
	public ResponseEntity<List<ProductoDTO>> searchByKeyword(@RequestParam String keyword,
			Pageable pageable) {
		Page<ProductoDTO> page = this.productoService.searchByKeyword(keyword, pageable);
		HttpHeaders response = new HttpHeaders();
		response.add(Constants.XTOTALCOUNT, String.valueOf(page.getTotalElements()));
		return ResponseEntity.ok().headers(response).body(page.getContent());
	}
}
