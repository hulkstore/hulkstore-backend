package com.hulk.store.config;

public class Constants {
    
    public static final String LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$";

    public static final String ADMIN_ACCOUNT = "ADMINISTRADOR"; // usuario
    public static final String SPANISH_LANGUAGE = "es";

    public static final String XTOTALCOUNT = "X-total-count"; // para paginacion

    private Constants() {
    }
}
