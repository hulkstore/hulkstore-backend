package com.hulk.store.config.dbmigrations;

import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.hulk.store.domain.CategoriaProducto;
import com.hulk.store.domain.Color;
import com.hulk.store.domain.EtiquetaProducto;
import com.hulk.store.domain.Producto;
import com.hulk.store.domain.ProductoCamiseta;
import com.hulk.store.domain.ProductoComic;
import com.hulk.store.domain.Rol;
import com.hulk.store.domain.Talla;
import com.hulk.store.domain.Usuario;
import com.hulk.store.dto.ProductoCamisetaDTO;
import com.hulk.store.dto.ProductoComicDTO;
import com.hulk.store.dto.ProductoDTO;
import com.hulk.store.dto.UsuarioDTO;
import com.hulk.store.repository.CategoriaProductoRepository;
import com.hulk.store.repository.ColorRepository;
import com.hulk.store.repository.EtiquetaProductoRepository;
import com.hulk.store.repository.ProductoCamisetaRepository;
import com.hulk.store.repository.ProductoComicRepository;
import com.hulk.store.repository.ProductoRepository;
import com.hulk.store.repository.RolRepository;
import com.hulk.store.repository.TallaRepository;
import com.hulk.store.repository.UsuarioRepository;
import com.hulk.store.service.mapper.ProductoCamisetaMapper;
import com.hulk.store.service.mapper.ProductoComicMapper;
import com.hulk.store.service.mapper.ProductoMapper;
import com.hulk.store.service.mapper.UsuarioMapper;

@Component
public class InitialSetupMigration implements ApplicationRunner  {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private UsuarioMapper usuarioMapper;
    @Autowired
    private RolRepository rolRepository;
    @Autowired
    private ColorRepository colorRepository;
    @Autowired
    private TallaRepository tallaRepository;
    @Autowired
    private CategoriaProductoRepository categoriaProductoRepository;
    @Autowired
    private EtiquetaProductoRepository etiquetaProductoRepository;
    @Autowired
    private ProductoRepository productoRepository;
    @Autowired
    private ProductoMapper productoMapper;
    @Autowired
    private ProductoCamisetaRepository productoCamisetaRepository;
    @Autowired
    private ProductoCamisetaMapper productoCamisetaMapper;
    @Autowired
    private ProductoComicRepository productoComicRepository;
    @Autowired
    private ProductoComicMapper productoComicMapper;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // TODO Auto-generated method stub
        // Long nro = usuarioRepository.count();
        Long nro = rolRepository.count();
        if (Integer.parseInt(nro.toString()) == 0) {
            // ----------------------------------------------------------------
            // roles
            Rol rol = new Rol();
            rol.setNombre("ADMINISTRADOR");
            rolRepository.save(rol);

            Rol rol1 = new Rol();
            rol1.setNombre("CLIENTE");
            rolRepository.save(rol1);
            // ----------------------------------------------------------------

            // ----------------------------------------------------------------
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            // usuarios
            UsuarioDTO usuarioDTO = new UsuarioDTO();
            usuarioDTO.setNombre("administrador");
            usuarioDTO.setNroTarjeta((long) 2332323);
            usuarioDTO.setUsername("admin");
            String userPassword = passwordEncoder.encode("admin");
            usuarioDTO.setPassword(userPassword);
            usuarioDTO.setEmail("administrador@localhost.com");
            usuarioDTO.setEstado(true);
            Set<String> roles = new HashSet<>();
            roles.add("ADMINISTRADOR");
            roles.add("CLIENTE");
            usuarioDTO.setRoles(roles);
            usuarioDTO.setCreatedBy("admin");
            usuarioDTO.setCreatedDate(Instant.now());
            usuarioDTO.setLastModifiedBy(null);
            usuarioDTO.setLastModifiedDate(null);
            this.usuarioRepository.save(usuarioMapper.toEntity(usuarioDTO));

            UsuarioDTO usuarioDTO2 = new UsuarioDTO();
            usuarioDTO2.setNombre("cliente");
            usuarioDTO2.setNroTarjeta((long) 2332323);
            usuarioDTO2.setUsername("cliente");
            userPassword = passwordEncoder.encode("cliente");
            usuarioDTO2.setPassword(userPassword);
            usuarioDTO2.setEmail("cliente@localhost.com");
            usuarioDTO2.setEstado(true);
            Set<String> roles1 = new HashSet<>();
            roles1.add("CLIENTE");
            usuarioDTO2.setRoles(roles1);
            usuarioDTO2.setCreatedBy("admin");
            usuarioDTO2.setCreatedDate(Instant.now());
            usuarioDTO2.setLastModifiedBy(null);
            usuarioDTO2.setLastModifiedDate(null);
            this.usuarioRepository.save(usuarioMapper.toEntity(usuarioDTO2));
            // ----------------------------------------------------------------
            // COLOR ----------------------------------------------------------
            Vector<String> vector = new Vector<String>();
            vector.add("Amarillo patito");
            vector.add("Verde militar");
            vector.add("Azul marino");
            vector.add("Azul oscuro");
            vector.add("Rojo");
            vector.add("Rosado");
            vector.add("Naranja");
            vector.add("Negro");
            vector.add("Plomo");
            vector.add("Gris");
            vector.add("Cafe madera");

            for (int i = 0; i < vector.size() ; i++) {
                Color color = new Color();
                color.setNombre(vector.elementAt(i));
                color.setCodigo("C" + String.valueOf((i+1)));
                color.setCreatedBy("admin");
                color.setCreatedDate(Instant.now());
                color.setLastModifiedBy(null);
                color.setLastModifiedDate(null);
                colorRepository.save(color);
            }

            Vector<String> vector1 = new Vector<String>();
            vector1.add("XS");
            vector1.add("S");
            vector1.add("M");
            vector1.add("L");
            vector1.add("XL");
            vector1.add("XXL");

            for (int i = 0; i < vector1.size() ; i++) {
                Talla talla = new Talla();
                talla.setTalla(vector1.elementAt(i));
                talla.setCreatedBy("admin");
                talla.setCreatedDate(Instant.now());
                talla.setLastModifiedBy(null);
                talla.setLastModifiedDate(null);
                tallaRepository.save(talla);
            }

            Vector<String> vector2 = new Vector<String>();
            vector2.add("VASO");
            vector2.add("CAMISETA");
            vector2.add("COMICS");
            vector2.add("JUEGUETES");
            vector2.add("ACCESORIOS");

            for (int i = 0; i < vector2.size() ; i++) {
                CategoriaProducto categoriaProducto = new CategoriaProducto();
                categoriaProducto.setNombre(vector2.elementAt(i));
                categoriaProducto.setCreatedBy("admin");
                categoriaProducto.setCreatedDate(Instant.now());
                categoriaProducto.setLastModifiedBy(null);
                categoriaProducto.setLastModifiedDate(null);
                categoriaProductoRepository.save(categoriaProducto);
            }

            Vector<String> vector3 = new Vector<String>();
            vector3.add("DC COMIC");
            vector3.add("MARVEL COMIC");
            vector3.add("COMUNIDAD");

            for (int i = 0; i < vector3.size() ; i++) {
                EtiquetaProducto etiquetaProducto = new EtiquetaProducto();
                etiquetaProducto.setNombre(vector3.elementAt(i));
                etiquetaProducto.setDescripcion(vector.elementAt(i));
                etiquetaProducto.setCreatedBy("admin");
                etiquetaProducto.setCreatedDate(Instant.now());
                etiquetaProducto.setLastModifiedBy(null);
                etiquetaProducto.setLastModifiedDate(null);
                etiquetaProductoRepository.save(etiquetaProducto);
            }

            ProductoDTO productoDTO = new ProductoDTO();
            productoDTO.setId(null);
            productoDTO.setNombre("VASO");
            productoDTO.setDescripcion("VASO DE BEBIDAS");
            productoDTO.setCodigo("VS1");
            productoDTO.setCostoUnitarioCompra(20.00);
            productoDTO.setPrecioUnitarioVenta(24.50);
            productoDTO.setCantidad(40);
            productoDTO.setEstado(true);
            productoDTO.setCreatedBy("admin");
            productoDTO.setCreatedDate(Instant.now());
            productoDTO.setLastModifiedBy(null);
            productoDTO.setLastModifiedDate(null);
            productoRepository.save(productoMapper.toEntity(productoDTO));

            
            productoDTO = new ProductoDTO();
            productoDTO.setId(null);
            productoDTO.setNombre("POLERA THOR MANGA LARGA");
            productoDTO.setDescripcion("POLERA THOR");
            productoDTO.setCodigo("PTML1");
            productoDTO.setCostoUnitarioCompra(15.00);
            productoDTO.setPrecioUnitarioVenta(25.50);
            productoDTO.setCantidad(10);
            productoDTO.setEstado(true);
            productoDTO.setCreatedBy("admin");
            productoDTO.setCreatedDate(Instant.now());
            productoDTO.setLastModifiedBy(null);
            productoDTO.setLastModifiedDate(null);
            Producto producto1 = productoRepository.save(productoMapper.toEntity(productoDTO));

            ProductoCamisetaDTO productoCamisetaDTO = new ProductoCamisetaDTO(); // ONE TO ONE PRODUCT
            productoCamisetaDTO.setId(null);
            productoCamisetaDTO.setSexo("MUJER");
            productoCamisetaDTO.setCreatedBy("admin");
            productoCamisetaDTO.setCreatedDate(Instant.now());
            productoCamisetaDTO.setLastModifiedBy(null);
            productoCamisetaDTO.setLastModifiedDate(null);
            productoCamisetaDTO.setColorId(null);
            productoCamisetaDTO.setTallaId(null);
            productoCamisetaDTO.setProductoId(producto1.getId());
            productoCamisetaRepository.save(productoCamisetaMapper.toEntity(productoCamisetaDTO));

            productoDTO = new ProductoDTO();
            productoDTO.setId(null);
            productoDTO.setNombre("COMICS THOR LOVE AND THUNDER VOL1");
            productoDTO.setDescripcion("REVISTA DE LECTURA, ENTRETENIMIENTO");
            productoDTO.setCodigo("CTLATV1");
            productoDTO.setCostoUnitarioCompra(40.00);
            productoDTO.setPrecioUnitarioVenta(45.00);
            productoDTO.setCantidad(20);
            productoDTO.setEstado(true);
            productoDTO.setCreatedBy("admin");
            productoDTO.setCreatedDate(Instant.now());
            productoDTO.setLastModifiedBy(null);
            productoDTO.setLastModifiedDate(null);
            producto1 = productoRepository.save(productoMapper.toEntity(productoDTO));

            ProductoComicDTO productoComicDTO = new ProductoComicDTO(); // ONE TO ONE PRODUCT
            productoComicDTO.setId(null);
            productoComicDTO.setTitulo("NOSE");
            productoComicDTO.setGenero("TERROR");
            productoComicDTO.setVolumen(1);
            productoComicDTO.setAnioLanzamiento(1999);
            productoComicDTO.setIdioma("ES");
            productoComicDTO.setCreatedBy("admin");
            productoComicDTO.setCreatedDate(Instant.now());
            productoComicDTO.setLastModifiedBy(null);
            productoComicDTO.setLastModifiedDate(null);
            productoComicDTO.setProductoId(producto1.getId());
            productoComicRepository.save(productoComicMapper.toEntity(productoComicDTO));

            productoDTO = new ProductoDTO();
            productoDTO.setId(null);
            productoDTO.setNombre("PERSONAJE BATMAN");
            productoDTO.setDescripcion("MUNECO DE COLECCION");
            productoDTO.setCodigo("PB1");
            productoDTO.setCostoUnitarioCompra(100.00);
            productoDTO.setPrecioUnitarioVenta(150.00);
            productoDTO.setCantidad(20);
            productoDTO.setEstado(true);
            productoDTO.setCreatedBy("admin");
            productoDTO.setCreatedDate(Instant.now());
            productoDTO.setLastModifiedBy(null);
            productoDTO.setLastModifiedDate(null);
            productoRepository.save(productoMapper.toEntity(productoDTO));

            productoDTO = new ProductoDTO();
            productoDTO.setId(null);
            productoDTO.setNombre("MENILLAS NEGRAS");
            productoDTO.setDescripcion("ACCESORIO PARA MANO");
            productoDTO.setCodigo("VN1");
            productoDTO.setCostoUnitarioCompra(10.00);
            productoDTO.setPrecioUnitarioVenta(12.00);
            productoDTO.setCantidad(20);
            productoDTO.setEstado(true);
            productoDTO.setCreatedBy("admin");
            productoDTO.setCreatedDate(Instant.now());
            productoDTO.setLastModifiedBy(null);
            productoDTO.setLastModifiedDate(null);
            productoRepository.save(productoMapper.toEntity(productoDTO));
            
        }
    }
    
}
