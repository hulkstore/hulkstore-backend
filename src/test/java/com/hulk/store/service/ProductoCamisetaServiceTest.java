package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.ProductoCamiseta;
import com.hulk.store.repository.ProductoCamisetaRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class ProductoCamisetaServiceTest {
    
    @Autowired
    private ProductoCamisetaRepository productoCamisetaRepository;

    ProductoCamiseta productoCamiseta;

    @BeforeEach
    public void init() {
        productoCamiseta = new ProductoCamiseta();
		productoCamiseta.setSexo("UNISEX");
        productoCamiseta.setCreatedBy("admin");
        productoCamiseta.setCreatedDate(Instant.now());
        productoCamiseta.setLastModifiedBy("admin");
        productoCamiseta.setLastModifiedDate(Instant.now());
		productoCamiseta.setColor(null);
		productoCamiseta.setTalla(null);
		productoCamiseta.setProducto(null);
        productoCamiseta = this.productoCamisetaRepository.save(productoCamiseta);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = productoCamiseta.getId();
        assertNotNull(this.productoCamisetaRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<ProductoCamiseta> list = productoCamisetaRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = productoCamiseta.getId();
		ProductoCamiseta ProductoCamiseta = this.productoCamisetaRepository.findById(id).get();
		assertEquals("UNISEX", ProductoCamiseta.getSexo());
	}
		
	@Test
	@Order(4)
	public void updateProductoCamisetaTest () {
        Long id = productoCamiseta.getId();
		ProductoCamiseta ProductoCamiseta = productoCamisetaRepository.findById(id).get();
		productoCamiseta.setSexo("MUJER");
        productoCamiseta.setCreatedBy("admin");
        productoCamiseta.setCreatedDate(Instant.now());
        productoCamiseta.setLastModifiedBy("admin");
        productoCamiseta.setLastModifiedDate(Instant.now());
		productoCamiseta.setColor(null);
		productoCamiseta.setTalla(null);
		productoCamiseta.setProducto(null);
		productoCamisetaRepository.save(ProductoCamiseta);
		assertNotEquals("UNISEX", productoCamisetaRepository.findById(id).get().getSexo());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = productoCamiseta.getId();
		productoCamisetaRepository.deleteById(id);
		assertThat(productoCamisetaRepository.existsById(id)).isFalse();
	}

}
