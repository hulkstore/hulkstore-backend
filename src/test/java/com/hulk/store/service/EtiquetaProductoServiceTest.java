package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.EtiquetaProducto;
import com.hulk.store.repository.EtiquetaProductoRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class EtiquetaProductoServiceTest {
    
    @Autowired
    private EtiquetaProductoRepository etiquetaProductoRepository;

    EtiquetaProducto etiquetaProducto;

    @BeforeEach
    public void init() {
        etiquetaProducto = new EtiquetaProducto();
        etiquetaProducto.setNombre("DC UNIVERSO");
        etiquetaProducto.setCreatedBy("admin");
        etiquetaProducto.setCreatedDate(Instant.now());
        etiquetaProducto.setLastModifiedBy("admin");
        etiquetaProducto.setLastModifiedDate(Instant.now());
        etiquetaProducto = this.etiquetaProductoRepository.save(etiquetaProducto);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = etiquetaProducto.getId();
        assertNotNull(this.etiquetaProductoRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<EtiquetaProducto> list = etiquetaProductoRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = etiquetaProducto.getId();
		EtiquetaProducto etiquetaProducto = this.etiquetaProductoRepository.findById(id).get();
		assertEquals("DC UNIVERSO", etiquetaProducto.getNombre() );
	}
		
	@Test
	@Order(4)
	public void updateetiquetaProductoTest () {
        Long id = etiquetaProducto.getId();
		EtiquetaProducto etiquetaProducto = etiquetaProductoRepository.findById(id).get();
        etiquetaProducto.setNombre("DC MULTIVERSO");
        etiquetaProducto.setCreatedBy("admin");
        etiquetaProducto.setCreatedDate(Instant.now());
        etiquetaProducto.setLastModifiedBy("admin");
        etiquetaProducto.setLastModifiedDate(Instant.now());
		etiquetaProductoRepository.save(etiquetaProducto);
		assertNotEquals("DC UNIVERSO", etiquetaProductoRepository.findById(id).get().getNombre());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = etiquetaProducto.getId();
		etiquetaProductoRepository.deleteById(id);
		assertThat(etiquetaProductoRepository.existsById(id)).isFalse();
	}

}
