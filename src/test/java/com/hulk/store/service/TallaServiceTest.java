package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Talla;
import com.hulk.store.repository.TallaRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class TallaServiceTest {
    
    @Autowired
    private TallaRepository tallaRepository;

    Talla talla;

    @BeforeEach
    public void init() {
        talla = new Talla();
        talla.setTalla("XLLL");
        talla.setCreatedBy("admin");
        talla.setCreatedDate(Instant.now());
        talla.setLastModifiedBy("admin");
        talla.setLastModifiedDate(Instant.now());
        talla = this.tallaRepository.save(talla);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = talla.getId();
        assertNotNull(this.tallaRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<Talla> list = tallaRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = talla.getId();
		Talla Talla = this.tallaRepository.findById(id).get();
		assertEquals("XLLL", Talla.getTalla() );
	}
		
	@Test
	@Order(4)
	public void updateTallaTest () {
        Long id = talla.getId();
		Talla Talla = tallaRepository.findById(id).get();
        Talla.setTalla("WWWw");
        Talla.setCreatedBy("admin");
        Talla.setCreatedDate(Instant.now());
        Talla.setLastModifiedBy("admin");
        Talla.setLastModifiedDate(Instant.now());
		tallaRepository.save(Talla);
		assertNotEquals("XLLL", tallaRepository.findById(id).get().getTalla());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = talla.getId();
		tallaRepository.deleteById(id);
		assertThat(tallaRepository.existsById(id)).isFalse();
	}

}
