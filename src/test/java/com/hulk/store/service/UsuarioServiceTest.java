package com.hulk.store.service;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Rol;
import com.hulk.store.domain.Usuario;
import com.hulk.store.repository.RolRepository;
import com.hulk.store.repository.UsuarioRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class UsuarioServiceTest {
    
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private RolRepository rolRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    Usuario usuario;

    @BeforeEach
    public void init() {
        Rol rol = new Rol();
        rol.setNombre("SUPER_USUARIO");
        rolRepository.save(rol);
        usuario = new Usuario();
		usuario.setNombre("MAYDA");
		usuario.setNroTarjeta(3323222L);
		usuario.setUsername("mayda");
		usuario.setPassword(passwordEncoder.encode("password")); // encode password
		usuario.setEmail("mayda@localhost.com");
		usuario.setEstado(true);
        Set<Rol> roles = new HashSet<Rol>();
        Rol auth = new Rol();
        auth.setNombre("SUPER_USUARIO");
        roles.add(auth);
        usuario.setRoles(roles);
        usuario.setCreatedBy("admin");
        usuario.setCreatedDate(Instant.now());
        usuario.setLastModifiedBy("admin");
        usuario.setLastModifiedDate(Instant.now());
        usuario = this.usuarioRepository.save(usuario);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = usuario.getId();
        assertNotNull(this.usuarioRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<Usuario> list = usuarioRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = usuario.getId();
		Usuario usuario = this.usuarioRepository.findById(id).get();
		assertEquals("mayda", usuario.getUsername());
	}
		
	@Test
	@Order(4)
	public void updateUsuarioTest () {
        Long id = usuario.getId();
		Usuario Usuario = usuarioRepository.findById(id).get();
		usuario.setNombre("MAYDA");
		usuario.setNroTarjeta(3323222L);
		usuario.setUsername("maydastore");
		usuario.setPassword(passwordEncoder.encode("password"));
		usuario.setEmail("mayda@localhost.com");
		usuario.setEstado(true);
        Set<Rol> roles = new HashSet<Rol>();
        Rol auth = new Rol();
        auth.setNombre("ADMINISTRADOR");
        roles.add(auth);
        usuario.setRoles(roles);
        usuario.setCreatedBy("admin");
        usuario.setCreatedDate(Instant.now());
        usuario.setLastModifiedBy("admin");
        usuario.setLastModifiedDate(Instant.now());
		usuarioRepository.save(usuario);
		assertNotEquals("mayda", usuarioRepository.findById(id).get().getUsername());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = usuario.getId();
		usuarioRepository.deleteById(id);
		assertThat(usuarioRepository.existsById(id)).isFalse();
	}

}
