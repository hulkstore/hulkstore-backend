package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.InvDetalleSalida;
import com.hulk.store.repository.InvDetalleSalidaRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class InvDetalleSalidaServiceTest {
    
    @Autowired
    private InvDetalleSalidaRepository invDetalleSalidaRepository;

    InvDetalleSalida invDetalleSalida;

    @BeforeEach
    public void init() {
        invDetalleSalida = new InvDetalleSalida();
		invDetalleSalida.setCantidad(4);
		invDetalleSalida.setPrecioUnitarioVenta(80.00);
        invDetalleSalida.setCreatedBy("admin");
        invDetalleSalida.setCreatedDate(Instant.now());
        invDetalleSalida.setLastModifiedBy("admin");
        invDetalleSalida.setLastModifiedDate(Instant.now());
        invDetalleSalida = this.invDetalleSalidaRepository.save(invDetalleSalida);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = invDetalleSalida.getId();
        assertNotNull(this.invDetalleSalidaRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<InvDetalleSalida> list = invDetalleSalidaRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = invDetalleSalida.getId();
		InvDetalleSalida InvDetalleSalida = this.invDetalleSalidaRepository.findById(id).get();
		assertEquals(80.00, InvDetalleSalida.getPrecioUnitarioVenta());
	}
		
	@Test
	@Order(4)
	public void updateInvDetalleSalidaTest () {
        Long id = invDetalleSalida.getId();
		InvDetalleSalida invDetalleSalida = invDetalleSalidaRepository.findById(id).get();
		invDetalleSalida.setCantidad(5);
		invDetalleSalida.setPrecioUnitarioVenta(100.00);
        invDetalleSalida.setCreatedBy("admin");
        invDetalleSalida.setCreatedDate(Instant.now());
        invDetalleSalida.setLastModifiedBy("admin");
        invDetalleSalida.setLastModifiedDate(Instant.now());
		invDetalleSalidaRepository.save(invDetalleSalida);
		assertNotEquals(80.00, invDetalleSalidaRepository.findById(id).get().getPrecioUnitarioVenta());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = invDetalleSalida.getId();
		invDetalleSalidaRepository.deleteById(id);
		assertThat(invDetalleSalidaRepository.existsById(id)).isFalse();
	}
    
}
