package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.InvDetalleEntrada;
import com.hulk.store.repository.InvDetalleEntradaRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class InvDetalleEntradaServiceTest {

    @Autowired
    private InvDetalleEntradaRepository invDetalleEntradaRepository;

    InvDetalleEntrada invDetalleEntrada;

    @BeforeEach
    public void init() {
        invDetalleEntrada = new InvDetalleEntrada();
		invDetalleEntrada.setCantidad(4);
		invDetalleEntrada.setCostoUnitario(100.00);
        invDetalleEntrada.setCreatedBy("admin");
        invDetalleEntrada.setCreatedDate(Instant.now());
        invDetalleEntrada.setLastModifiedBy("admin");
        invDetalleEntrada.setLastModifiedDate(Instant.now());
        invDetalleEntrada = this.invDetalleEntradaRepository.save(invDetalleEntrada);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = invDetalleEntrada.getId();
        assertNotNull(this.invDetalleEntradaRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<InvDetalleEntrada> list = invDetalleEntradaRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = invDetalleEntrada.getId();
		InvDetalleEntrada InvDetalleEntrada = this.invDetalleEntradaRepository.findById(id).get();
		assertEquals(100.00, InvDetalleEntrada.getCostoUnitario());
	}
		
	@Test
	@Order(4)
	public void updateInvDetalleEntradaTest () {
        Long id = invDetalleEntrada.getId();
		InvDetalleEntrada invDetalleEntrada = invDetalleEntradaRepository.findById(id).get();
		invDetalleEntrada.setCantidad(5);
		invDetalleEntrada.setCostoUnitario(120.00);
        invDetalleEntrada.setCreatedBy("admin");
        invDetalleEntrada.setCreatedDate(Instant.now());
        invDetalleEntrada.setLastModifiedBy("admin");
        invDetalleEntrada.setLastModifiedDate(Instant.now());
		invDetalleEntradaRepository.save(invDetalleEntrada);
		assertNotEquals(100.00, invDetalleEntradaRepository.findById(id).get().getCostoUnitario());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = invDetalleEntrada.getId();
		invDetalleEntradaRepository.deleteById(id);
		assertThat(invDetalleEntradaRepository.existsById(id)).isFalse();
	}

}
