package com.hulk.store.service;


import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.InvSalida;
import com.hulk.store.repository.InvSalidaRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class InvSalidaServiceTest {

    @Autowired
    private InvSalidaRepository invSalidaaRepository;

    InvSalida invSalida;

    @BeforeEach
    public void init() {
        invSalida = new InvSalida();
		invSalida.setDescuento(0.00);
		invSalida.setMontoTotal(40.00);
		invSalida.setTipoPago("TARJETA");
		invSalida.setEstado(true);
        invSalida.setCreatedBy("admin");
        invSalida.setCreatedDate(Instant.now());
        invSalida.setLastModifiedBy("admin");
        invSalida.setLastModifiedDate(Instant.now());
        invSalida = this.invSalidaaRepository.save(invSalida);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = invSalida.getId();
        assertNotNull(this.invSalidaaRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<InvSalida> list = invSalidaaRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = invSalida.getId();
		InvSalida InvSalida = this.invSalidaaRepository.findById(id).get();
		assertEquals("TARJETA", InvSalida.getTipoPago());
	}
		
	@Test
	@Order(4)
	public void updateInvSalidaTest () {
        Long id = invSalida.getId();
		InvSalida InvSalida = invSalidaaRepository.findById(id).get();
		invSalida.setDescuento(0.00);
		invSalida.setMontoTotal(40.00);
		invSalida.setTipoPago("TARJETA PLATINO");
		invSalida.setEstado(true);
        InvSalida.setCreatedBy("admin");
        InvSalida.setCreatedDate(Instant.now());
        InvSalida.setLastModifiedBy("admin");
        InvSalida.setLastModifiedDate(Instant.now());
		invSalidaaRepository.save(InvSalida);
		assertNotEquals("TARJETA", invSalidaaRepository.findById(id).get().getTipoPago());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = invSalida.getId();
		invSalidaaRepository.deleteById(id);
		assertThat(invSalidaaRepository.existsById(id)).isFalse();
	}

}
