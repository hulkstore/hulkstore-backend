package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.CategoriaProducto;
import com.hulk.store.repository.CategoriaProductoRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class CategoriaProductoServiceTest {
    
    @Autowired
    private CategoriaProductoRepository categoriaProductoRepository;

    CategoriaProducto categoriaProducto;

    @BeforeEach
    public void init() {
        categoriaProducto = new CategoriaProducto();
        categoriaProducto.setNombre("COMUNIDAD COMIC");
        categoriaProducto.setCreatedBy("admin");
        categoriaProducto.setCreatedDate(Instant.now());
        categoriaProducto.setLastModifiedBy("admin");
        categoriaProducto.setLastModifiedDate(Instant.now());
        categoriaProducto = this.categoriaProductoRepository.save(categoriaProducto);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = categoriaProducto.getId();
        assertNotNull(this.categoriaProductoRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<CategoriaProducto> list = categoriaProductoRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = categoriaProducto.getId();
		CategoriaProducto categoriaProducto = this.categoriaProductoRepository.findById(id).get();
		assertEquals("COMUNIDAD COMIC", categoriaProducto.getNombre() );
	}
		
	@Test
	@Order(4)
	public void updatecategoriaProductoTest () {
        Long id = categoriaProducto.getId();
		CategoriaProducto categoriaProducto = categoriaProductoRepository.findById(id).get();
        categoriaProducto.setNombre("COMUNIDAD COMIC AUTO");
        categoriaProducto.setCreatedBy("admin");
        categoriaProducto.setCreatedDate(Instant.now());
        categoriaProducto.setLastModifiedBy("admin");
        categoriaProducto.setLastModifiedDate(Instant.now());
		categoriaProductoRepository.save(categoriaProducto);
		assertNotEquals("COMUNIDAD COMIC", categoriaProductoRepository.findById(id).get().getNombre());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = categoriaProducto.getId();
		categoriaProductoRepository.deleteById(id);
		assertThat(categoriaProductoRepository.existsById(id)).isFalse();
	}

}
