package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Color;
import com.hulk.store.repository.ColorRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class ColorServiceTest {
    
    @Autowired
    private ColorRepository colorRepository;

    Color color;

    @BeforeEach
    public void init() {
        color = new Color();
        color.setNombre("ROJO");
        color.setCodigo("R20");
        color.setCreatedBy("admin");
        color.setCreatedDate(Instant.now());
        color.setLastModifiedBy("admin");
        color.setLastModifiedDate(Instant.now());
        color = this.colorRepository.save(color);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = color.getId();
        assertNotNull(this.colorRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<Color> list = colorRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = color.getId();
		Color color = this.colorRepository.findById(id).get();
		assertEquals("R20", color.getCodigo());
	}
		
	@Test
	@Order(4)
	public void updateColorTest () {
        Long id = color.getId();
		Color color = colorRepository.findById(id).get();
        color.setNombre("VERDE");
        color.setCodigo("V1");
        color.setCreatedBy("admin");
        color.setCreatedDate(Instant.now());
        color.setLastModifiedBy("admin");
        color.setLastModifiedDate(Instant.now());
		colorRepository.save(color);
		assertNotEquals("R20", colorRepository.findById(id).get().getCodigo());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = color.getId();
		colorRepository.deleteById(id);
		assertThat(colorRepository.existsById(id)).isFalse();
	}

}
