package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Imagen;
import com.hulk.store.repository.ImagenRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class ImagenServiceTest {
    @Autowired
    private ImagenRepository imagenRepository;

    Imagen imagen;

    @BeforeEach
    public void init() {
        imagen = new Imagen();
		imagen.setNombre("imagen1");
		imagen.setUrl("/imagen.jpg");
		imagen.setTipo("jpg");
        imagen.setCreatedBy("admin");
        imagen.setCreatedDate(Instant.now());
        imagen.setLastModifiedBy("admin");
        imagen.setLastModifiedDate(Instant.now());
        imagen = this.imagenRepository.save(imagen);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = imagen.getId();
        assertNotNull(this.imagenRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<Imagen> list = imagenRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = imagen.getId();
		Imagen Imagen = this.imagenRepository.findById(id).get();
		assertEquals("/imagen.jpg", Imagen.getUrl());
	}
		
	@Test
	@Order(4)
	public void updateImagenTest () {
        Long id = imagen.getId();
		Imagen imagen = imagenRepository.findById(id).get();
		imagen.setNombre("imagen1");
		imagen.setUrl("/imagen.png");
		imagen.setTipo("png");
        imagen.setCreatedBy("admin");
        imagen.setCreatedDate(Instant.now());
        imagen.setLastModifiedBy("admin");
        imagen.setLastModifiedDate(Instant.now());
		assertNotEquals("/imagen.jpg", imagenRepository.findById(id).get().getUrl());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = imagen.getId();
		imagenRepository.deleteById(id);
		assertThat(imagenRepository.existsById(id)).isFalse();
	}   
}
