package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.InvEntrada;
import com.hulk.store.repository.InvEntradaRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class InvEntradaServiceTest {
    
    @Autowired
    private InvEntradaRepository invEntradaRepository;

    InvEntrada invEntrada;

    @BeforeEach
    public void init() {
        invEntrada = new InvEntrada();
		invEntrada.setEstado(true);
		invEntrada.setTotal(500.00);
        invEntrada.setCreatedBy("admin");
        invEntrada.setCreatedDate(Instant.now());
        invEntrada.setLastModifiedBy("admin");
        invEntrada.setLastModifiedDate(Instant.now());
        invEntrada = this.invEntradaRepository.save(invEntrada);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = invEntrada.getId();
        assertNotNull(this.invEntradaRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<InvEntrada> list = invEntradaRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = invEntrada.getId();
		InvEntrada invEntrada = this.invEntradaRepository.findById(id).get();
		assertEquals(500.00, invEntrada.getTotal());
	}
		
	@Test
	@Order(4)
	public void updateInvEntradaTest () {
        Long id = invEntrada.getId();
		InvEntrada InvEntrada = invEntradaRepository.findById(id).get();
		invEntrada.setEstado(true);
		invEntrada.setTotal(600.00);
        InvEntrada.setCreatedBy("admin");
        InvEntrada.setCreatedDate(Instant.now());
        InvEntrada.setLastModifiedBy("admin");
        InvEntrada.setLastModifiedDate(Instant.now());
		invEntradaRepository.save(InvEntrada);
		assertNotEquals(500.00, invEntradaRepository.findById(id).get().getTotal());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = invEntrada.getId();
		invEntradaRepository.deleteById(id);
		assertThat(invEntradaRepository.existsById(id)).isFalse();
	}
    
}
