package com.hulk.store.service;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Rol;
import com.hulk.store.repository.RolRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class RolServiceTest {

    @Autowired
    private RolRepository rolRepository;

    Rol rol;

    @BeforeEach
    public void init() {
        rol = new Rol();
        rol.setNombre("SUPER_ADMINISTRADOR");
        rol = this.rolRepository.save(rol);
    }

    @Test
	@Order(1)
    public void createTest() {
        String id = rol.getNombre();
        assertNotNull(this.rolRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<Rol> list = rolRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        String id = rol.getNombre();
		Rol Rol = this.rolRepository.findById(id).get();
		assertEquals("SUPER_ADMINISTRADOR", Rol.getNombre());
	}
		
	@Test
	@Order(4)
	public void updateRolTest () {
        String id = rol.getNombre();
		Rol rol = rolRepository.findById(id).get();
        rol.setNombre("ROL_TODO");
		rolRepository.save(rol);
		assertNotEquals("SUPER_ADMINISTRADOR", rolRepository.findById(id).get().getNombre());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        String id = rol.getNombre();
		rolRepository.deleteById(id);
		assertThat(rolRepository.existsById(id)).isFalse();
	}

}
