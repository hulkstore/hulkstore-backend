package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.Producto;
import com.hulk.store.repository.ProductoRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional

public class ProductoServiceTest {

    @Autowired
    private ProductoRepository productoRepository;

    Producto producto;

    @BeforeEach
    public void init() {
        producto = new Producto();
        producto.setNombre("CAMISETA X");
		producto.setDescripcion("CAMISETA X");
		producto.setCodigo("CX1");
		producto.setCostoUnitarioCompra(20.50);
		producto.setPrecioUnitarioVenta(25.00);
		producto.setCantidad(40);
		producto.setEstado(true);
        producto.setCreatedBy("admin");
        producto.setCreatedDate(Instant.now());
        producto.setLastModifiedBy("admin");
        producto.setLastModifiedDate(Instant.now());
        producto = this.productoRepository.save(producto);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = producto.getId();
        assertNotNull(this.productoRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<Producto> list = productoRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = producto.getId();
		Producto Producto = this.productoRepository.findById(id).get();
		assertEquals("CAMISETA X", Producto.getNombre());
	}
		
	@Test
	@Order(4)
	public void updateProductoTest () {
        Long id = producto.getId();
		Producto Producto = productoRepository.findById(id).get();
        producto.setNombre("CAMISETA Y");
		producto.setDescripcion("CAMISETA X");
		producto.setCodigo("CX1");
		producto.setCostoUnitarioCompra(20.50);
		producto.setPrecioUnitarioVenta(25.00);
		producto.setCantidad(40);
		producto.setEstado(true);
        Producto.setCreatedBy("admin");
        Producto.setCreatedDate(Instant.now());
        Producto.setLastModifiedBy("admin");
        Producto.setLastModifiedDate(Instant.now());
		productoRepository.save(Producto);
		assertNotEquals("CAMISETA X", productoRepository.findById(id).get().getNombre());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = producto.getId();
		productoRepository.deleteById(id);
		assertThat(productoRepository.existsById(id)).isFalse();
	}

}
