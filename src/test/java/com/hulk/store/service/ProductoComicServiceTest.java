package com.hulk.store.service;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.hulk.store.domain.ProductoComic;
import com.hulk.store.repository.ProductoComicRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
@Transactional
public class ProductoComicServiceTest {
    @Autowired
    private ProductoComicRepository productoComicRepository;

    ProductoComic productoComic;

    @BeforeEach
    public void init() {
        productoComic = new ProductoComic();
		productoComic.setTitulo("COMIC FINAL");
		productoComic.setGenero("ACCION");
		productoComic.setVolumen(1);
		productoComic.setAnioLanzamiento(2006);
		productoComic.setIdioma("ES");
        productoComic.setCreatedBy("admin");
        productoComic.setCreatedDate(Instant.now());
        productoComic.setLastModifiedBy("admin");
        productoComic.setLastModifiedDate(Instant.now());
		productoComic.setProducto(null);
        productoComic = this.productoComicRepository.save(productoComic);
    }

    @Test
	@Order(1)
    public void createTest() {
        Long id = productoComic.getId();
        assertNotNull(this.productoComicRepository.findById(id).get());
    }

    @Test
	@Order(2)
	public void findAllTest () {
		List<ProductoComic> list = productoComicRepository.findAll();
		assertThat(list).size().isGreaterThan(0);
	}

    @Test
	@Order(3)
	public void findOneByIdTest () {
        Long id = productoComic.getId();
		ProductoComic productoComic = this.productoComicRepository.findById(id).get();
		assertEquals("ES", productoComic.getIdioma());
	}
		
	@Test
	@Order(4)
	public void updateProductoComicTest () {
        Long id = productoComic.getId();
		ProductoComic ProductoComic = productoComicRepository.findById(id).get();
		productoComic.setTitulo("COMIC FINAL");
		productoComic.setGenero("ACCION");
		productoComic.setVolumen(1);
		productoComic.setAnioLanzamiento(2006);
		productoComic.setIdioma("EN");
		ProductoComic.setProducto(null);
		productoComicRepository.save(ProductoComic);
		assertNotEquals("ES", productoComicRepository.findById(id).get().getIdioma());
	}
		
	@Test
	@Order(5)
	public void deleteTest () {
        Long id = productoComic.getId();
		productoComicRepository.deleteById(id);
		assertThat(productoComicRepository.existsById(id)).isFalse();
	}

}
