-- TABLA ROL
INSERT INTO rol(nombre, created_by, created_date, last_modified_by, last_modified_date) VALUES('ADMINISTRADOR', 'admin', '2022-06-26', null, null);
INSERT INTO rol(nombre, created_by, created_date, last_modified_by, last_modified_date) VALUES('CLIENTE', 'admin', '2022-06-26', null, null);
-- TABLA USUARIO
INSERT INTO usuario(id, nombre, nro_tarjeta, username, password_hash, email, estado, created_by, created_date, last_modified_by, last_modified_date) VALUES(1, 'Nestor', 232323, 'admin', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'admin@localhost.com', true, 'admin', '2022-06-26', null, null);
INSERT INTO usuario(id, nombre, nro_tarjeta, username, password_hash, email, estado, created_by, created_date, last_modified_by, last_modified_date) VALUES(2, 'Maria', 232321, 'cliente', '$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K', 'cliente@localhost.com', true, 'admin', '2022-06-26', null, null);
-- TABLA INTERMEDIA USUARIO ROL
INSERT INTO usuario_rol(usuario_id, rol_nombre) VALUES (1, 'ADMINISTRADOR');
INSERT INTO usuario_rol(usuario_id, rol_nombre) VALUES (1, 'CLIENTE');
INSERT INTO usuario_rol(usuario_id, rol_nombre) VALUES (2, 'CLIENTE');
-- TABLA COLOR
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (1, 'VERDE LIMON', 'VL1', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (2, 'VERDE MILITAR', 'VM1', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (3, 'VERDE OSCURO', 'VO1', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (4, 'CELESTE CIELO', 'CC1', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (5, 'CELESTE CLARO', 'CC2', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (6, 'AZUL MARINO', 'AM1', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (7, 'ROSADO', 'R1', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (8, 'ROJO', 'R2', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (9, 'AMARILLO PATITO', 'AP1', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (10, 'GRIS', 'G1', 'admin', '2022-06-26', null, null);
INSERT INTO color(id, nombre, codigo, created_by, created_date, last_modified_by, last_modified_date) VALUES (11, 'PLOMO', 'P1', 'admin', '2022-06-26', null, null);
-- TABLA TALLA
INSERT INTO talla(id, talla, created_by, created_date, last_modified_by, last_modified_date) VALUES (1, 'XS', 'admin', '2022-06-26', null, null);
INSERT INTO talla(id, talla, created_by, created_date, last_modified_by, last_modified_date) VALUES (2, 'S', 'admin', '2022-06-26', null, null);
INSERT INTO talla(id, talla, created_by, created_date, last_modified_by, last_modified_date) VALUES (3, 'M', 'admin', '2022-06-26', null, null);
INSERT INTO talla(id, talla, created_by, created_date, last_modified_by, last_modified_date) VALUES (4, 'L', 'admin', '2022-06-26', null, null);
INSERT INTO talla(id, talla, created_by, created_date, last_modified_by, last_modified_date) VALUES (5, 'XL', 'admin', '2022-06-26', null, null);
INSERT INTO talla(id, talla, created_by, created_date, last_modified_by, last_modified_date) VALUES (6, 'XXL', 'admin', '2022-06-26', null, null);
-- TABLA ETIQUETA PRODUCTO
INSERT INTO etiquetaproducto(id, nombre, descripcion, created_by, created_date, last_modified_by, last_modified_date) VALUES (1, 'DC COMIC', 'DC COMIC', 'admin', '2022-06-26', null, null);
INSERT INTO etiquetaproducto(id, nombre, descripcion, created_by, created_date, last_modified_by, last_modified_date) VALUES (2, 'MAVERL COMIC', 'MARVEL COMIC', 'admin', '2022-06-26', null, null);
INSERT INTO etiquetaproducto(id, nombre, descripcion, created_by, created_date, last_modified_by, last_modified_date) VALUES (3, 'COMUNIDAD', 'COMUNIDAD', 'admin', '2022-06-26', null, null);
-- TABLA CATEGORIA PRODUCTO
INSERT INTO categoriaproducto(id, nombre, created_by, created_date, last_modified_by, last_modified_date) VALUES (1, 'VASO', 'admin', '2022-06-26', null, null);
INSERT INTO categoriaproducto(id, nombre, created_by, created_date, last_modified_by, last_modified_date) VALUES (2, 'CAMISETA', 'admin', '2022-06-26', null, null);
INSERT INTO categoriaproducto(id, nombre, created_by, created_date, last_modified_by, last_modified_date) VALUES (3, 'COMICS', 'admin', '2022-06-26', null, null);
INSERT INTO categoriaproducto(id, nombre, created_by, created_date, last_modified_by, last_modified_date) VALUES (4, 'JUGUETES', 'admin', '2022-06-26', null, null);
INSERT INTO categoriaproducto(id, nombre, created_by, created_date, last_modified_by, last_modified_date) VALUES (5, 'ACCESORIOS X', 'admin', '2022-06-26', null, null);
-- TABLA PRODUCTO
INSERT INTO producto(id, nombre, descripcion, codigo, costo_unitario_compra, precio_unitario_venta, cantidad, estado, categoria_id, etiqueta_id, created_by, created_date, last_modified_by, last_modified_date) VALUES (1, 'VASO SORBETE', 'VASO DE BEBIDAS', 'VS1', 20.50, 20.00, 40, true, 1, 1, 'admin', '2022-06-26', null, null);
INSERT INTO producto(id, nombre, descripcion, codigo, costo_unitario_compra, precio_unitario_venta, cantidad, estado, categoria_id, etiqueta_id, created_by, created_date, last_modified_by, last_modified_date) VALUES (2, 'POLERA THOR MANGA LARGA', 'PLERA THOR', 'VS1', 20.50, 20.00, 40, true, 2, 2, 'admin', '2022-06-26', null, null);
INSERT INTO producto(id, nombre, descripcion, codigo, costo_unitario_compra, precio_unitario_venta, cantidad, estado, categoria_id, etiqueta_id, created_by, created_date, last_modified_by, last_modified_date) VALUES (3, 'COMICS THOR LOVE AND THUNDER VOL1', 'REVISTA DE LECTURA, ENTRETENIMIENTO', 'VS1', 20.50, 20.00, 40, true, 3, 2, 'admin', '2022-06-26', null, null);
INSERT INTO producto(id, nombre, descripcion, codigo, costo_unitario_compra, precio_unitario_venta, cantidad, estado, categoria_id, etiqueta_id, created_by, created_date, last_modified_by, last_modified_date) VALUES (4, 'PERSONAJE BATMAN', 'MUNECO DE COLECCION', 'VS1', 20.50, 20.00, 40, true, 4, 2, 'admin', '2022-06-26', null, null);
INSERT INTO producto(id, nombre, descripcion, codigo, costo_unitario_compra, precio_unitario_venta, cantidad, estado, categoria_id, etiqueta_id, created_by, created_date, last_modified_by, last_modified_date) VALUES (5, 'MENILLAS NEGRAS', 'ACCESORIO PARA MANO', 'VS1', 20.50, 20.00, 40, true, 5, 3, 'admin', '2022-06-26', null, null);
